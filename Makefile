IMAGE?=rubentxu/jenkins-dev-container
TAG?=latest

BUILDER=buildx-multi-arch

INFO_COLOR = \033[0;36m
NO_COLOR   = \033[m

exec: ## Run service image
	docker exec -it $(shell docker ps -qf "ancestor=$(IMAGE):$(TAG)") /bin/ash

exec-curl: ## Run service image
	docker exec -it $(shell docker ps -qf "ancestor=$(IMAGE):$(TAG)") curl -v -GET --unix-socket /run/guest-services/backend.sock http://localhost/status

run-image: ## Run service image
	docker run -d --rm -p 8083:32433 $(IMAGE):$(TAG)

stop-container: ## Stop service image
	docker stop $(shell docker ps -qf "ancestor=$(IMAGE):$(TAG)")

remove-image: ## Remove service image
	docker rmi $(IMAGE):$(TAG) -f

stop-remove-all: 
	docker stop $(shell docker ps -q) 
	docker rmi $(shell docker images -q) -f

remove-all: ## Remove all images
	docker rmi $(shell docker images -q) -f || true
	remove-cache
	
build-extension: ## Build service image to be deployed as a desktop extension
	docker build --tag=$(IMAGE):$(TAG) .

install-extension: build-extension ## Install the extension
	docker extension install $(IMAGE):$(TAG)

update-extension: build-extension ## Update the extension
	docker extension update $(IMAGE):$(TAG)

remove-extension: ## Remove the extension
	docker extension remove $(IMAGE):$(TAG) || true
	docker rmi $(IMAGE):$(TAG) -f

remove-cache: ## Remove all docker cache
	docker builder prune -f

prepare-buildx: ## Create buildx builder for multi-arch build, if not exists
	docker buildx inspect $(BUILDER) || docker buildx create --name=$(BUILDER) --driver=docker-container --driver-opt=network=host

push-extension: prepare-buildx ## Build & Upload extension image to hub. Do not push if tag already exists: make push-extension tag=0.1
	docker pull $(IMAGE):$(TAG) && echo "Failure: Tag already exists" || docker buildx build --push --builder=$(BUILDER) --platform=linux/amd64,linux/arm64 --build-arg TAG=$(TAG) --tag=$(IMAGE):$(TAG) .

help: ## Show this help
	@echo Please specify a build target. The choices are:
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "$(INFO_COLOR)%-30s$(NO_COLOR) %s\n", $$1, $$2}'

.PHONY: help

compile-web:
	cd ui/ && flutter build web --web-renderer canvaskit --release

backend-web-sources: compile-web
	rm -fr backend/web/	
	cp -R ui/build/web backend/

compile-app: backend-web-sources
	cd backend/ && go build -trimpath -ldflags="-s -w" -o build/jdc
	cp backend/.env backend/build/.env	

run-app:
	cd backend/build && ./jdc


  
