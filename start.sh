#!/bin/sh

# Create the sock file
# nc -lU /run/guest-services/backend.sock &

# # Create the volume
# mkdir -p /run/guest-services

# sed -i 's/listen  .*/listen 32433;/g' /etc/nginx/conf.d/default.conf

# Start the main process
exec nginx -g 'daemon off;'
