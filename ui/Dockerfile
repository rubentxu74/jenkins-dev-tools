FROM golang:1.19-alpine AS builder
ENV CGO_ENABLED=0
WORKDIR /backend
COPY backend/go.* .
RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go mod download
COPY backend/. .
RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go build -trimpath -ldflags="-s -w" -o bin/service

FROM --platform=$BUILDPLATFORM ubuntu:22.04 AS client-builder
RUN apt-get update
RUN apt-get install -y curl git wget unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa fonts-droid-fallback lib32stdc++6 python3
RUN apt-get clean
RUN git clone https://github.com/flutter/flutter.git /usr/local/flutter
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"
RUN flutter doctor
RUN flutter channel master
RUN flutter upgrade
RUN flutter config --enable-web
WORKDIR /ui
COPY ui /ui
RUN flutter build web

FROM nginx:1.21.1-alpine
RUN apk add --no-cache --update openrc netcat-openbsd && \
    mkdir -p /run/guest-services

COPY --from=builder /backend/bin/service /
COPY --from=client-builder /ui/build/web/ /usr/share/nginx/html
COPY ui/context/start.sh /start.sh
RUN chmod +x /start.sh
EXPOSE 80

ENTRYPOINT ["/start.sh"]