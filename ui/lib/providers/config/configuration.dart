import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

var logger = Logger();

class Configuration {
  static const String _configFilePath = 'assets/config';

  Configuration(this._configMap);

  final Map<String, String> _configMap;

  static Future<Configuration> load(BuildContext context) async {
    var profile = const String.fromEnvironment('FLUTTER_PROFILE',
        defaultValue: 'default');
    String envFileName = "${_configFilePath}/.env-${profile}";
    try {
      logger.d("Loading configuration from ${envFileName}");
      final envFileContent =
          await DefaultAssetBundle.of(context).loadString(envFileName);
      var config = parseEnvFile(envFileContent);
      return Configuration(config);
    } catch (e) {
      throw Exception(
          "Configuration file ${envFileName} not found, please create it.");
    }
  }

  static Map<String, String> parseEnvFile(String fileContent) {
    var lines = fileContent.split('\n');
    var env = <String, String>{};
    for (var line in lines) {
      if (line.startsWith('#') || line.isEmpty) {
        continue;
      }
      var parts = line.split('=');
      env[parts[0].trim()] = parts[1].trim();
    }
    return env;
  }

  String resolve(String key) {
    // get value from environment
    var envValue = String.fromEnvironment(key, defaultValue: '');
    logger.d("envValue: ${envValue}");
    return !envValue.isEmpty
        ? envValue
        : _configMap.putIfAbsent(key, () => 'unix');
  }

  String get socketAddressType => resolve('SOCKET_ADDRESS_TYPE');
}
