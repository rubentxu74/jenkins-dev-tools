import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ui/api/api.dart';

import 'plugins_provider.dart';

typedef ConsoleFunction = TextSpan? Function(
    String log,
    ConsoleProvider console,
    BuildContext context,
    );


class ConsoleProvider with ChangeNotifier {
  WebsocketRPCApi client;
  final int limitLogs;
  List<String> _logs = [];
  bool isComplete = false;

  ConsoleProvider({this.limitLogs = 200, required this.client }) {}

  void clearLogs() {
    _logs.clear();
    notifyListeners();
  }

  Future<bool> executeConsoleAction(SocketRequest request) async {
    try {
      return await client.executeAction(request, addLog, errorFunc: addLogsError);
    } catch (ex) {
      addLogsError(ex);
    }
    return false;

  }

  void onDone() {
    print("On done console action");
  }

  Future<bool> installAsdfTool() async {
    return await client.installAsdfTool(addLog, errorFunc: addLogsError);
  }

  Future<bool> installJenkinsPlugins(List<String> plugins) async {
    return await client.installJenkinsPlugins(plugins, addLog, errorFunc: addLogsError);
  }

  Future<bool> runJenkinsContainer(String jenkinsVersion, String libraryDirectory) async {
    return await client.runJenkinsContainer(jenkinsVersion, libraryDirectory, addLog, errorFunc: addLogsError);
  }


  addLogsError(ex) {
    _logs.add('----------------------------------------');
    _logs.add('Error Console logs: $ex');
    _logs.add('----------------------------------------');
    print('Error Console logs: $ex');
    notifyListeners();
  }

  void done() {
    // client.disconnect();
    // onDone();
  }

  int get length => _logs.length;

  List<String> get logs => _logs;

  void addLog(dynamic message) {
    final logLines = message.split('\n');

    for (final line in logLines) {
      if (line.trim() != '') {
        dynamic parsedLine;
        try {
          parsedLine = jsonDecode(line);
          if(parsedLine['stream'] != null)  {
            _logs.add(parsedLine['stream'].trim());
          } else if (parsedLine['body'] != null) {
            _logs.add(parsedLine['body'].trim());
          } else {
            _logs.add(line.trim());
          }
        } catch (e) {
          _logs.add(line.trim());
        }
        if (_logs.length > limitLogs) {
          _logs.removeRange(0, _logs.length - limitLogs);
          _logs.removeRange(0, 2);
        }
      }
    }
    notifyListeners();
  }

  @override
  void dispose() {
    // client.disconnect();
    super.dispose();
  }

  void reset() {
    _logs = [];
    isComplete = false;
    notifyListeners();
  }
}
