import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:ui/providers/builders/request_builder.dart';


var streamTransformer = StreamTransformer<Uint8List, Map>.fromHandlers(
  handleData: (Uint8List data, EventSink sink) {
    var result = String.fromCharCodes(data);
    // extract json from response
    var json = result.substring(result.indexOf("{"), result.lastIndexOf("}") + 1);
    // convert json to map
    Map map = jsonDecode(json);
    sink.add(map);

  },
  handleError: (Object error, StackTrace stacktrace, EventSink sink) {
    sink.addError({ error : 'Something went wrongr: $error' });
  },
  handleDone: (EventSink sink) => sink.close(),
);

class SocketProvider with ChangeNotifier {
  static const String socketUnixPath = "/run/guest-services/backend.sock";
  static const String socketInternetPath = "http://localhost:8081";
  Socket? socket;
  bool connected = false;
  static Map<String, String> headers = {
    "Host": "localhost",
    "User-Agent": "curl/7.87.0",
    "Accept": "*/*"
  };


  bool get isConnected => connected;

  InternetAddress getAddress(String socketType) {
    if(socketType == "unix") {
      return InternetAddress(socketUnixPath, type: InternetAddressType.unix);
    } else {
      return InternetAddress(socketInternetPath, type: InternetAddressType.IPv4);
    }
  }

  Future<void> connect(String socketType) async {
    try {
      socket = await Socket.connect('http://localhost',8081);

      var request = RequestBuilder.buildGet("/status", headers, {});
      socket?.write(request);
      socket?.transform(streamTransformer).listen((Map result) {
        if(result != null && result.containsKey("message") && result["message"] == "Status OK" ) {
          print("Connected");
          connected = true;
          notifyListeners();
        } else {
          print("Disconnected");
          connected = false;
        }
        // notifyListeners();
      }).asFuture();
    } catch(e) {
      print("Socket connection error $e");
    }

  }

  Future<void> disconnect() async {
    socket?.close();
    connected = false;
    notifyListeners();
  }

  void close() {
    socket?.close();
    notifyListeners();
  }

  // Function for making GET request to the server
  Future<String> getData(String url, Map<String, String> headers, Map<String, dynamic> queryStrings) async {
    String request = RequestBuilder.buildGet(url, headers, queryStrings);
    socket?.write(request);
    var result =  await socket?.transform(streamTransformer).join();
    return result ?? "";
  }

  // Function for making POST request to the server
  Future<String> postData(String url, Map<String, String> headers, String body) async {
    String request = RequestBuilder.buildPost(url, headers, body);
    socket?.write(request);
    var result =  await socket?.transform(streamTransformer).join();
    return result ?? "";

  }

  // Function for making PUT request to the server
  Future<String> putData(String url, Map<String, String> headers, String body) async {
    String request = RequestBuilder.buildPut(url, headers, body);
    socket?.write(request);
    var result =  await socket?.transform(streamTransformer).join();
    return result ?? "";
  }

  // Function for making DELETE request to the server
  Future<String> deleteData(String url, Map<String, String> headers) async {
    String request = RequestBuilder.buildDelete(url, headers);
    socket?.write(request);
    var result =  await socket?.transform(streamTransformer).join();
    return result ?? "";
  }
}
