import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ui/api/api.dart';
import 'package:ui/api/asdf_tool_service.dart';
import 'package:ui/datasources/asdf_tools.dart';
import 'package:ui/models/models.dart';
import 'package:ui/services/services.dart';

class AsdfProvider extends ChangeNotifier {
  AsdfToolService _asdfToolService;
  final searchController = TextEditingController();


  List<AsdfTool> _defaultAsdfTools = [];
  late List<AsdfTool> _installedAsdfTools;
  List<AsdfTool> _allAsdfTools = [];
  List<AsdfTool> _filteredAsdfTools = [];
  bool _isLoading = false;

  
  AsdfProvider({required AsdfToolService asdfToolService})
      : _asdfToolService = asdfToolService {
    LocalStorage.prefs.getString('asdfListInstalledAsdfTools') != null
        ? _installedAsdfTools = (json.decode(
                LocalStorage.prefs.getString('asdfListInstalledAsdfTools')!) as List)
            .map((e) => AsdfTool.fromJson(e))
            .toList()
        : _installedAsdfTools = [];

  }

  void filterData(String searchText) {
    _filteredAsdfTools = _allAsdfTools.where((tool) =>
        tool.name.toLowerCase().contains(searchText)).toList();
    notifyListeners();
  }

  Future<void>  getAvailableTools() async  {
    if (_allAsdfTools.isEmpty) {
      _isLoading = true;
      await _asdfToolService.getTools().then((value) {
        _allAsdfTools = value;
        _isLoading = false;
      });
    }
  }

  Future<void>  getDefaultTools() async  {
    if (_defaultAsdfTools.isEmpty) {
      _isLoading = true;
      await _asdfToolService.defaultTools().then((value) {
        _defaultAsdfTools = value;
        _isLoading = false;
      });
    }
  }

  int getSize() {
    return _allAsdfTools.length;
  }

  List<AsdfTool> get defaultAsdfTools => _defaultAsdfTools;
  List<AsdfTool> get allAsdfTools => _allAsdfTools;
  List<AsdfTool> get filteredAsdfTools => _filteredAsdfTools;
  List<AsdfTool> get installedAsdfTools => _installedAsdfTools;
  bool get isLoading => _isLoading;


}
