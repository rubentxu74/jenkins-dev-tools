class RequestBuilder {

  static String buildPost(String url, Map<String, String> headers, String body) {
    String request = "POST $url HTTP/1.1\r\n";
    headers.forEach((key, value) {
      request += "$key: $value\r\n";
    });

    if (body.isNotEmpty) {
      request += "Content-Length: ${body.length}\r\n\r\n";
    } else {
      request += "\r\n";
    }
    return request;
  }

  static String buildGet(String url, Map<String, String> headers, Map<String, dynamic> queryStrings) {
    queryStrings.forEach((key, value) {
      var keyUrlFriendly = stringUrlFriendly(key);
      var valueUrlFriendly = stringUrlFriendly(value);
      url += "?$keyUrlFriendly=$valueUrlFriendly";
    });
    String request = "GET $url HTTP/1.1\r\n";
    headers.forEach((key, value) {
      request += "$key: $value\r\n";
    });
    request += "\r\n";
    return String.fromCharCodes(request.codeUnits);

  }
  
  static String stringUrlFriendly(String value) {
     return value.replaceAll(" ", "%20")
         .replaceAll("!", "%21")
         .replaceAll("#", "%23")
         .replaceAll("\$", "%24")
         .replaceAll("&", "%26")
         .replaceAll("'", "%27")
         .replaceAll("(", "%28")
         .replaceAll(")", "%29")
         .replaceAll("*", "%2A")
         .replaceAll("+", "%2B")
         .replaceAll(",", "%2C")
         .replaceAll("/", "%2F")
         .replaceAll(":", "%3A")
         .replaceAll(";", "%3B")
         .replaceAll("=", "%3D")
         .replaceAll("?", "%3F")
         .replaceAll("@", "%40")
         .replaceAll("[", "%5B")
         .replaceAll("]", "%5D");
  }

  static String buildPut(String url, Map<String, String> headers, String body) {
    String request = "PUT $url HTTP/1.1\r\n";
    headers.forEach((key, value) {
      request += "$key: $value\r\n";
    });

    if (body.isNotEmpty) {
      request += "Content-Length: ${body.length}\r\n\r\n";
    } else {
      request += "\r\n";
    }
    return request;
  }

  static String buildDelete(String url, Map<String, String> headers) {
    String request = "DELETE $url HTTP/1.1\r\n";
    headers.forEach((key, value) {
      request += "$key: $value\r\n";
    });    
    return request;
  }

  static String buildPatch(String url, Map<String, String> headers, String body) {
    String request = "PATCH $url HTTP/1.1\r\n";
    headers.forEach((key, value) {
      request += "$key: $value\r\n";
    });

    if (body.isNotEmpty) {
      request += "Content-Length: ${body.length}\r\n\r\n";
    } else {
      request += "\r\n";
    }
    return request;
  }




}
