import 'package:flutter/material.dart';
import 'package:ui/models/models.dart';

class SetupJenkins extends ChangeNotifier {

  List<String> _jenkinsVersionList = [
    '2.375.3-lts-jdk11',
    '2.361.4-lts-jdk11',
    '2.346.3-2-lts-jdk11'

  ];

  late String _jenkinsVersion = '2.375.3-lts-jdk11';
  late bool _installPodmanIsChecked = false;
  late bool _installPythonIsChecked = false;
  late bool _createdImage = false;
  int _currentStep = 0;
  StepperType _stepperType = StepperType.horizontal;
  late bool _installAsdfIsChecked = false;
  late bool _installDefaultPluginsIsChecked = false;
  late String _libraryDirectory = "";

  String get libraryDirectory => _libraryDirectory??"";

  void setLibraryDirectory(String? newPath) {
    _libraryDirectory = newPath??"";
    notifyListeners();
  }


  List<String> get jenkinsVersionList => this._jenkinsVersionList;

  set jenkinsVersion(String value) {
    _jenkinsVersion = value;
    notifyListeners();
  }

  String get jenkinsVersion => _jenkinsVersion;

  set installPodmanIsChecked(bool value) {
    _installPodmanIsChecked = value;
    notifyListeners();
  }

  bool get installPodmanIsChecked => _installPodmanIsChecked;

  set installPythonIsChecked(bool value) {
    _installPythonIsChecked = value;
    notifyListeners();
  }

  bool get installPythonIsChecked => _installPythonIsChecked;

  set currentStep(int value) {
    _currentStep = value;
    notifyListeners();
  }

  int get currentStep => _currentStep;

  set stepperType(StepperType value) {
    _stepperType = value;
    notifyListeners();
  }

  StepperType get stepperType => _stepperType;

  set createdImage(bool value) {
    _createdImage = value;
  }

  bool get createdImage => _createdImage;

  set installAsdfIsChecked(bool value) {
    _installAsdfIsChecked = value;
  }

  bool get installAsdfIsChecked => _installAsdfIsChecked;

  set installDefaultPluginsIsChecked(bool value) {
    _installDefaultPluginsIsChecked = value;
  }

  bool get installDefaultPluginsIsChecked => _installDefaultPluginsIsChecked;

}


