import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ui/api/api.dart';
import 'package:ui/models/models.dart';
import 'package:ui/services/services.dart';

class PluginsProvider extends ChangeNotifier {
  PluginsService _pluginsService;
  final searchController = TextEditingController();

  List<Plugin> _defaultPlugins = [];
  List<Plugin> _availablePlugins = [];
  late List<Plugin> _installedPlugins;
  List<Plugin> _allPlugins = [];
  List<Plugin> _filteredPlugins = [];
  bool _isLoading = false;

  PluginsProvider({required PluginsService pluginsService})
      : _pluginsService = pluginsService {
    LocalStorage.prefs.getString('listInstalledPlugins') != null
        ? _installedPlugins =
            (json.decode(LocalStorage.prefs.getString('listInstalledPlugins')!)
                    as List)
                .map((e) => Plugin.fromJson(e))
                .toList()
        : _installedPlugins = [];
  }

  void filterData(String searchText) {
    _filteredPlugins = _allPlugins
        .where((plugin) => plugin.name.toLowerCase().contains(searchText))
        .toList();
    notifyListeners();
  }

  List<Plugin> get allPlugins => _allPlugins;

  List<Plugin> get filteredPlugins => _filteredPlugins;

  List<Plugin> get defaultPlugins => _defaultPlugins;

  List<Plugin> get availablePlugins => _availablePlugins;

  List<Plugin> get installedPlugins => _installedPlugins;

  bool get isLoading => _isLoading;

  void set isLoading(bool isLoading) {
    _isLoading = isLoading;
    notifyListeners();
  }

  Future<void> getAllPlugins() async {
    if (_allPlugins.isEmpty) {
      _isLoading = true;
      _allPlugins = await _pluginsService.getAllPlugins();
      _isLoading = false;
      notifyListeners();
    }
  }

  int getSize() {
    return _allPlugins.length;
  }

  Future<void> getInstalledPlugins() async {
    if (_allPlugins.isEmpty) {
      _isLoading = true;
      _installedPlugins = await _pluginsService.getInstalledPlugins();
      _isLoading = false;
      LocalStorage.prefs
          .setString('listInstalledPlugins', jsonEncode(_installedPlugins));
      LocalStorage.prefs.setBool('installedPlugins', true);
      notifyListeners();
    }
  }


  Future<void> getDefaultPlugins() async {
    if (_allPlugins.isEmpty) {
      _isLoading = true;
      _defaultPlugins = await _pluginsService.getDefaultPlugins();
      _isLoading = false;
      notifyListeners();
    }
  }

  void togglePlugin(int index) {
    _defaultPlugins[index].isSelected = !_defaultPlugins[index].isSelected;
    notifyListeners();
  }

  void updatePluginVersion(int index, String version) {
    _defaultPlugins[index].version = version;
    notifyListeners();
  }

  Future<void> installPlugins() async {
    _isLoading = true;
    notifyListeners();
    // await apiProvider.installPlugins(_defaultPlugins);
    _isLoading = false;
    notifyListeners();
  }

  Future<void> getAvailablePlugins() async {
    if (_allPlugins.isEmpty) {
      getAllPlugins();
    }
    if (_installedPlugins.isEmpty) {
      await getInstalledPlugins();
    }
    _availablePlugins = _availablePlugins
        .where((plugin) => !_installedPlugins
            .any((installedPlugin) => installedPlugin.name == plugin.name))
        .toList();
    notifyListeners();
  }

  void addPlugin(String text) {
    _defaultPlugins
        .add(Plugin(name: text, version: 'latest', isSelected: true));
    notifyListeners();
  }
}
