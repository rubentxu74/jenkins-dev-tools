import 'package:flutter/material.dart';
import 'package:ui/services/services.dart';

class ApplicationStatus  extends ChangeNotifier {
  bool _startedJenkins = false;

  String get currentJenkinsVersion  {
    return LocalStorage.prefs.getString('currentJenkinsVersion') ?? '';
  }

  bool get installedPodman {
    return LocalStorage.prefs.getBool('installedPodman') ?? false;
  }

  bool get installedPython {
    return LocalStorage.prefs.getBool('installedPython') ?? false;
  }

  bool get installedAsdf {
    return LocalStorage.prefs.getBool('installedAsdf') ?? false;
  }

  bool get installedPlugins {
    return LocalStorage.prefs.getBool('installedPlugins') ?? false;
  }

  bool get completedSetup {
    return LocalStorage.prefs.getBool('completedSetup') ?? false;
  }

  bool get startedJenkins {
    return _startedJenkins;
  }

  bool get createdImage {
    return LocalStorage.prefs.getBool('createdImage') ?? false;
  }

  String get libraryDirectory {
    return LocalStorage.prefs.getString('libraryDirectory') ?? '';
  }

  set currentJenkinsVersion(String value) {
    LocalStorage.prefs.setString('currentJenkinsVersion', value);
  }

  set installedPodman(bool value) {
    LocalStorage.prefs.setBool('installedPodman', value);
  }

  set installedPython(bool value) {
    LocalStorage.prefs.setBool('installedPython', value);
  }

  set installedAsdf(bool value) {
    LocalStorage.prefs.setBool('installedAsdf', value);
  }

  set installedPlugins(bool value) {
    LocalStorage.prefs.setBool('installedPlugins', value);
  }

  set startedJenkins(bool value) {
    _startedJenkins = value;
    notifyListeners();
  }

  set createdImage(bool value) {
    LocalStorage.prefs.setBool('createdImage', value);
  }

  set libraryDirectory(String value) {
    LocalStorage.prefs.setString('libraryDirectory', value);
  }

  set completedSetup(bool value) {
    LocalStorage.prefs.setBool('completedSetup', value);
  }

  void reset() {
    currentJenkinsVersion = '';
    installedPodman = false;
    installedPython = false;
    installedAsdf = false;
    installedPlugins = false;
    _startedJenkins = false;
    createdImage = false;
    libraryDirectory = '';
    completedSetup = false;
  }
}
