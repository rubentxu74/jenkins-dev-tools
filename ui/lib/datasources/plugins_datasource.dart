import 'package:flutter/material.dart';
import 'package:ui/api/api.dart';
import 'package:ui/models/models.dart';


class PluginDataSource extends DataTableSource {
  List<Plugin> _plugins;
  int _selectedCount = 0;
  final BuildContext context;
  bool isDefault;

  PluginDataSource(this._plugins, this.context, {this.isDefault = false});

  // Métodos requeridos por DataTableSource
  @override
  DataRow? getRow(int index) {
    if (index >= _plugins.length) return null;

    final plugin = _plugins[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text(plugin.name)),
        DataCell(Text(plugin.version)),
        if(!isDefault)
          DataCell(IconButton(
            icon: Icon(Icons.play_arrow),
            onPressed: () {
              // Acción que se realiza al presionar el botón
            },
          )),
      ],
      selected: plugin.isSelected,
      onSelectChanged: (value) {
        if (plugin.isSelected != value) {
          _selectedCount += value! ? 1 : -1;
          plugin.isSelected = value;
          notifyListeners();
        }
      },
    );
  }

  @override
  int get rowCount => _plugins.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
