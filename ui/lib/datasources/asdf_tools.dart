import 'package:flutter/material.dart';
import 'package:ui/models/models.dart';



class AsdfDataSource extends DataTableSource {
  List<AsdfTool> _tools;
  int _selectedCount = 0;
  final BuildContext context;
  bool isDefault;

  AsdfDataSource(this._tools, this.context, {this.isDefault = false});


  // Métodos requeridos por DataTableSource
  @override
  DataRow? getRow(int index) {
    if (index >= _tools.length) return null;

    final tool = _tools[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text(tool.name)),
        DataCell(Text(tool.version)),
        if(!isDefault)
          DataCell(IconButton(
            icon: Icon(Icons.play_arrow),
            onPressed: () {
              // Acción que se realiza al presionar el botón
            },
          )),
      ],
      selected: tool.isSelected,
      onSelectChanged: (value) {
        if (tool.isSelected != value) {
          _selectedCount += value! ? 1 : -1;
          tool.isSelected = value;
          notifyListeners();
        }
      },
    );
  }

  @override
  int get rowCount => _tools.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

