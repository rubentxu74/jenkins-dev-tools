import 'package:flutter/material.dart';
import 'package:ui/api/api.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/router/router.dart';
import 'package:ui/services/services.dart';

class ConnectionStatus extends StatelessWidget {
  final ApplicationStatus status;
  final WebsocketRPCApi rpcClient;

  ConnectionStatus({required this.status, required this.rpcClient });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Center(
          child: resolveIcon(),
        ),
        SizedBox(width: 10.0),
        Flexible(
          child: Text(
            resolveTitle(),
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ),
      ],
    );

  }

  String resolveTitle() {
    if(status.createdImage && !status.startedJenkins) {
      return 'Iniciar Jenkins Server';
    } else if (status.startedJenkins) {
      return 'Detener Jenkins Server';
    } else {
      return 'Construir la Imagen Jenkins Server';
    }
  }

  Widget resolveIcon() {
    if(status.createdImage && !status.startedJenkins) {
      return _buttonIconLed(Icon(Icons.power_settings_new, color: Colors.white,));
    } else if (status.startedJenkins) {
      return _startedButtonIconLed(Icon(Icons.power_settings_new, color: Colors.white,));
    } else {
      return _buttonIconLed(Icon(Icons.build_circle_outlined, color: Colors.white,));
    }
  }

  Widget _buttonIconLed(Icon icon) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Colors.red,
            Colors.orange,
          ],
        ),
        borderRadius: BorderRadius.circular(24.0),
      ),
      child: IconButton(
        icon: icon,
        onPressed: onPressedJenkinsButton
      ),
    );
  }

  void onPressedJenkinsButton() async {
        print("Status version: ${status.currentJenkinsVersion}");
        print("Status library: ${status.libraryDirectory}");
        print("Status image: ${status.createdImage}");
        print("Status started: ${status.startedJenkins}");
        print("Status completedSetup: ${status.completedSetup}");

        if(status.completedSetup && !status.startedJenkins) {
          print('Opcion 1');
          await rpcClient.runJenkinsContainer(status.currentJenkinsVersion,
              status.libraryDirectory, (event) => print(event), errorFunc: (e) => print('error'));
  
          var result = await rpcClient.healthCheckRemoteScriptInJenkins((event) { print(event); }, errorFunc: (e) => print('error'));
          if(result) {
  
            status.startedJenkins = true;
          } else {
            print('Jenkins Server is not running');
            status.startedJenkins = false;
          }
          return;
        } else if (status.completedSetup && status.startedJenkins) {
          print('Opcion 2');
          print('Jenkins Server is running');
          await rpcClient.stopJenkinsContainer((event) => print(event), errorFunc: (e) => print('error'));
          status.startedJenkins = false;
          print('Jenkins Server stopped');
          return;
        } else {
          print('Opcion 3');
          if(status.currentJenkinsVersion == '') {
            NavigationService.replaceTo(AppRouter.setupImageRoute);
            SideMenuProvider.closeMenu();
            return;
          } else if (status.libraryDirectory == '') {
            NavigationService.replaceTo(AppRouter.setupContainerRoute);
            SideMenuProvider.closeMenu();
            return;
          }
        }
      }
  
  

  Widget _startedButtonIconLed(Icon icon) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Colors.green,
            Colors.lightGreen,
          ],
        ),
        borderRadius: BorderRadius.circular(24.0),
      ),
      child: IconButton(
          icon: icon,
          onPressed: onPressedJenkinsButton
      ),
    );
  }

  _iconLed() {
    return IconButton(
      icon: Container(
        foregroundDecoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.orange.shade100, Colors.orange.shade900],
              begin: Alignment(0, 0),
              end: Alignment(0, 1),
            ),
            backgroundBlendMode: BlendMode.screen,
          borderRadius: BorderRadius.circular(24.0),
        ),
        child: Icon(
            Icons.power_settings_new,

        )
      ),
      iconSize: 36,
      color: Colors.black,
      onPressed: () {},
    );
  }
}
