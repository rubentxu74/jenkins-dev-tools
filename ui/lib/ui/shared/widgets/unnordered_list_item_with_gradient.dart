

import 'package:flutter/material.dart';
import 'package:ui/ui/shared/widgets/gradient_icon.dart';

class UnnorderedListItemWithGradient extends StatelessWidget {
  const UnnorderedListItemWithGradient({
    super.key,
    required this.title,
    this.iconSize = 8.0,
    this.iconData = Icons.circle_outlined,
    this.spacing = 5.0,
    this.gradient = const LinearGradient(
      colors: <Color>[
        Color(0xFF0D47A1),
        Color(0xFF1976D2),
        Color(0xFF42A5F5),
      ],
    ),
  });

  final TextSpan title;
  final double iconSize;
  final IconData iconData;
  final Gradient gradient;
  final double spacing;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        spacing: spacing,
        children: [
          GradientIcon(iconData, iconSize, gradient),
          RichText(
            text: title,
            overflow: TextOverflow.ellipsis,
            softWrap: true,
          ),
        ],
      ),
    );
  }
}