

import 'package:flutter/material.dart';

class UnnorderedListItem extends StatelessWidget {
  const UnnorderedListItem({
    super.key,
    required this.title,
    this.iconSize = 8.0,
    this.iconData = Icons.circle_outlined,
    this.spacing = 10.0,
    this.iconColor = Colors.grey,
  });

  final TextSpan title;
  final double iconSize;
  final IconData iconData;
  final MaterialColor iconColor;
  final double spacing;


  @override
  Widget build(BuildContext context) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: spacing,
      children: [
        Icon(iconData, size: iconSize),
        RichText(
          text: title,
          overflow: TextOverflow.ellipsis,
          softWrap: true,

        ),
      ],
    );
  }
}