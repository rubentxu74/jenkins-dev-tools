import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/router/router.dart';
import 'package:ui/services/services.dart';
import 'package:ui/ui/shared/shared.dart';

class Sidebar extends StatelessWidget {

  void navigateTo( String routeName ) {
    NavigationService.replaceTo( routeName );
    SideMenuProvider.closeMenu();
  }

  @override
  Widget build(BuildContext context) {

    final sideMenuProvider = Provider.of<SideMenuProvider>(context);

    return Container(
      width: widthSidebar,
      height: double.infinity,
      decoration: buildBoxDecoration(),
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: [

          Logo(),

          SizedBox( height: 50 ),

          TextSeparator( text: 'main' ),

          MenuItem(
            text: 'Dashboard',
            icon: Icons.compass_calibration_outlined,
            onPressed: () => navigateTo( AppRouter.dashboardRoute ),
            isActive: sideMenuProvider.currentPage == AppRouter.dashboardRoute,
          ),

          // MenuItem(
          //     text: 'Setup Jenkins',
          //     icon: Icons.phonelink_setup,
          //     onPressed:  () => navigateTo( AppRouter.setupRoute ),
          //     isActive: sideMenuProvider.currentPage == AppRouter.setupRoute,
          // ),
          MenuItem(
              text: 'Plugins',
              icon: Icons.show_chart_outlined,
              onPressed: () => navigateTo( AppRouter.pluginsRoute ),
              isActive: sideMenuProvider.currentPage == AppRouter.pluginsRoute,
          ),
          TextSeparator( text: 'Exit' ),
          MenuItem( 
            text: 'Logout', 
            icon: Icons.exit_to_app_outlined, 
            onPressed: (){

            }),
        ],
      ),
    );
  }

  BoxDecoration buildBoxDecoration() => BoxDecoration(
    gradient: LinearGradient(
      colors: [
        Color(0xFF384B56),
        Colors.blueGrey
      ]
    ),
    boxShadow: [
      BoxShadow(
        color: Colors.black26,
        blurRadius: 10
      )
    ]
  );
}