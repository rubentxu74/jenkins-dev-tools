import 'package:flutter/material.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/ui/shared/shared.dart';


class Navbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: 50,
      decoration: buildBoxDecoration(),
      child: Row(
        children: [
          if (size.width <= limitDesktop)
            IconButton(
                icon: Icon(Icons.menu_outlined),
                onPressed: () => SideMenuProvider.openMenu()),

          SizedBox(width: 5),

          // Search input
          // if (size.width > 390)
          //   ConstrainedBox(
          //     constraints: BoxConstraints(maxWidth: 250),
          //     child: SearchText(),
          //   ),

          Spacer(),

          NotificationsIndicator(),
          SizedBox(width: 10),
          // NavbarAvatar(),
          // SizedBox(width: 10)
        ],
      ),
    );
  }

  BoxDecoration buildBoxDecoration() => BoxDecoration(
      color: Colors.transparent,
      // boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 5)]
  );
}
