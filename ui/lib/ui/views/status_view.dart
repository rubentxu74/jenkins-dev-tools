import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/api/api.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/application_status.dart';
import 'package:ui/ui/shared/shared.dart';

class StatusView extends StatelessWidget {

  WebsocketRPCApi rpcApi;

  StatusView({Key? key, required this.rpcApi}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: size.height - heightNavbar,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: ListView(physics: ClampingScrollPhysics(), children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: buildStatusPanel(context, rpcApi),
            ),
            if (size.width > limitDesktop) ...[
              Container(
                width: 1.0,
                color: Colors.black,
                child: SizedBox(height: size.height - heightNavbar),
              ),
              Expanded(
                flex: 1,
                child: buildLogoPanel(),
              ),
            ]
          ],
        ),
        if (size.width <= limitDesktop) ...[buildLogoPanel()],
      ]),
    );
  }

  Container buildLogoPanel() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 50),
      alignment: Alignment.center,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: 350.0,
          maxWidth: 350.0,
        ),
        // Aquí puedes poner la ruta a tu imagen SVG
        child: Image.asset('assets/images/jenkins_512.png'),
      ),
    );
  }

  Container buildStatusPanel(BuildContext context, WebsocketRPCApi rpcApi) {
    var status = Provider.of<ApplicationStatus>(context, listen: true);
    return Container(
        child: Column(
            children: [
              Container(
                width: 330,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Estado del servicio',
                        style: TextStyle(fontSize: 30.0, color: Colors.black)),
                    SizedBox(height: 20.0),
                    Container(
                      width: double.infinity,
                      child: ConnectionStatus(
                        status: status,
                        rpcClient: rpcApi,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    _buildUnnorderedListItem(
                        'Version de Jenkins ${status.currentJenkinsVersion}',
                        !status.currentJenkinsVersion.isEmpty),
                    SizedBox(height: 20.0),
                    _buildUnnorderedListItem(
                        'Podman esta instalado ', status.installedPodman),
                    SizedBox(height: 20.0),
                    _buildUnnorderedListItem(
                        'Asdf esta instalado ', status.installedAsdf),
                    SizedBox(height: 20.0),
                    _buildUnnorderedListItem(
                        'Python esta instalado ', status.installedPython),
                    SizedBox(height: 20.0),
                    _buildUnnorderedListItem('Plugins de Jenkins instalados ',
                        status.installedPlugins),
                    SizedBox(height: 20.0),
                  ],
                ),
              ),
            ]));
  }

  Widget _buildUnnorderedListItem(String text, bool isInstalled) {
    return UnnorderedListItemWithGradient(
      iconSize: 20.0,
      iconData: Icons.circle_rounded,
      gradient: isInstalled
          ? LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.green,
                Colors.greenAccent,
              ],
            )
          : LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.redAccent,
                Colors.orange,
              ],
            ),
      title: TextSpan(
          text: text, style: TextStyle(fontSize: 15.0, color: Colors.black)),
    );
  }
}
