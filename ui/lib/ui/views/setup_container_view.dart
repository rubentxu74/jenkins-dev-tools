import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/asdf_provider.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/router/router.dart';
import 'package:ui/ui/shared/shared.dart';
import 'package:ui/ui/views/views.dart';

import 'steps/jenkins_context.dart';

class SetupContainerView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Consumer<SetupJenkins>(builder: (context, setup, child) {
      return ListView(
        physics: ClampingScrollPhysics(),
        children: [
          Container(
            color: Colors.transparent,
            margin: EdgeInsets.only(
              left: 10,
            ),
            width: size.width,
            height: size.height - heightNavbar,
            child: Stepper(
              type: size.width > 700
                  ? StepperType.horizontal
                  : StepperType.vertical,
              physics: ScrollPhysics(),
              currentStep: setup.currentStep,
              onStepTapped: (step) => {
                setup.currentStep = step,
              },
              onStepContinue: () => continued(setup, context),
              onStepCancel: () => cancel(setup),
              steps: <Step>[
                _buildStepOne(setup),
                _buildStepTwo(setup),
                _buildStepThree(context, setup),
              ],
              controlsBuilder: (BuildContext context, ControlsDetails details) {
                final _isLastStep = setup.currentStep == 3 - 1;
                return Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: Row(children: [
                      if (setup.currentStep != 0)
                        Container(
                            child: ElevatedButton(
                                child: Text('Atrás'),
                                onPressed: details.onStepCancel)),
                      const SizedBox(
                        width: 12,
                      ),
                      Container(
                          child: ElevatedButton(
                              child: Text(_isLastStep ? 'Build' : 'Continuar'),
                              onPressed: details.onStepContinue)),
                    ]));
              },
            ),
          ),
        ],
      );
    });
  }

  Step _buildStepThree(BuildContext context, SetupJenkins setup) {
    var asdf = Provider.of<AsdfProvider>(context, listen: false);
    return Step(
      title: new Text(
        'Libreria Groovy',
        overflow: TextOverflow.ellipsis,
      ),
      content: JenkinsContextStep(),
      isActive: setup.currentStep >= 0,
      state: setup.currentStep >= 2 ? StepState.complete : StepState.disabled,
    );
  }

  Step _buildStepTwo(SetupJenkins setup) {
    return Step(
      title: new Text(
        'Plugins de Jenkins',
        overflow: TextOverflow.ellipsis,
      ),
      content: PluginsView(
        title: "Plugins recomendados",
        isDefault: true,
      ),
      isActive: setup.currentStep >= 0,
      state: setup.currentStep >= 1 ? StepState.complete : StepState.disabled,
    );
  }

  Step _buildStepOne(SetupJenkins setup) {
    return Step(
      title: new Text('Antes de Ejecutar', overflow: TextOverflow.ellipsis),
      content: Container(
        // Long description
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
                'Necesitamos que nos proporciones algunos datos para poder ejecutar Jenkins.',
                overflow: TextOverflow.ellipsis),
            SizedBox(height: 20.0),
            UnnorderedListItem(
              title: TextSpan(
                  text:
                      'Instalaremos algunos plugins utiles para el funcionamiento de Jenkins.',
                  style: TextStyle(color: Colors.black)),
            ),
            UnnorderedListItem(
                title: TextSpan(
                    text:
                        'Luego te preguntaremos por el directorio donde se encuentra la libreria Groovy para Jenkins.')),
            SizedBox(height: 20.0),
            Text('Pasa a la siguiente etapa.', overflow: TextOverflow.ellipsis),
            SizedBox(height: 20.0),
          ],
        ),
      ),
      isActive: setup.currentStep >= 0,
      state: setup.currentStep >= 0 ? StepState.complete : StepState.disabled,
    );
  }

  switchStepsType(SetupJenkins setup) {
    setup.stepperType == StepperType.vertical
        ? setup.stepperType = StepperType.horizontal
        : setup.stepperType = StepperType.vertical;
  }

  continued(SetupJenkins setup, BuildContext context) {
    setup.currentStep < 2
        ? setup.currentStep += 1
        : navigateToConsole(setup, context);
  }

  cancel(SetupJenkins setup) {
    setup.currentStep > 0 ? setup.currentStep -= 1 : null;
  }

  void navigateToConsole(SetupJenkins setup, BuildContext context) async {
    Navigator.pushNamed(context, AppRouter.consoleJenkinsRoute);
    var console = Provider.of<ConsoleProvider>(context, listen: false);
    var pluginsProvider = Provider.of<PluginsProvider>(context, listen: false);

    console.reset();
    await console.runJenkinsContainer(
        setup.jenkinsVersion, setup.libraryDirectory);
    var status = Provider.of<ApplicationStatus>(context, listen: false);

    status.installedAsdf = await console.installAsdfTool();
    status.installedPlugins = await console.installJenkinsPlugins(
        pluginsProvider.defaultPlugins.map((e) => e.name).toList());
    status.startedJenkins = await console.runJenkinsContainer(
        setup.jenkinsVersion, setup.libraryDirectory);

    if(status.installedAsdf && status.installedPlugins && status.startedJenkins) {
      status.completedSetup = true;
    } else {
      status.completedSetup = false;
    }
    status.libraryDirectory = setup.libraryDirectory;
    console.isComplete = true;

    print( "Complete: ${console.isComplete}");
    setup.currentStep = 0;
  }
}
