import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/providers/providers.dart';

class JenkinsContextStep extends StatelessWidget {
  final TextEditingController _directoryController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _resetDirectory(BuildContext context) {
    Provider.of<SetupJenkins>(context, listen: false)
        .setLibraryDirectory('');
    _directoryController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SetupJenkins>(builder: (context, provider, child) {
      return Column(
        children: [
          Text(
              'Necesitamos que nos proporciones el directorio donde se encuentra la libreria de Jenkins',
              overflow: TextOverflow.ellipsis),
          SizedBox(height: 20.0),
          if (provider.libraryDirectory.isEmpty)
            Form(
              key: _formKey,
              child: TextFormField(
                controller: _directoryController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor, introduce la ruta del directorio';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: 'Ruta del directorio',
                ),
              ),
            ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              if (provider.libraryDirectory.isEmpty) ...[
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      provider.setLibraryDirectory(_directoryController.text);
                    }
                  },
                  child: Text('Guardar directorio'),
                ),
                SizedBox(
                  height: 80,
                ),
              ],
              if (provider.libraryDirectory.isNotEmpty)
                ElevatedButton(
                  onPressed: () => _resetDirectory(context),
                  child: Text('Reset'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                  ),
                ),
            ],
          ),
          if (provider.libraryDirectory.isNotEmpty) ...[
            SizedBox(
              height: 20,
            ),
            Text('Directorio seleccionado:', style: TextStyle( fontWeight: FontWeight.bold, fontSize: 20 )),
            SizedBox(height: 16,),
            Text('${provider.libraryDirectory}'),
            SizedBox(
              height: 20,
            ),
          ]
        ],
      );
    });
  }
}
