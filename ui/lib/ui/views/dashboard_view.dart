import 'package:flutter/material.dart';
import 'package:ui/ui/cards/white_card.dart';
import 'package:ui/ui/labels/custom_labels.dart';

class DashboardView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric( horizontal: 20, vertical: 10 ),
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: [
          Text('Dashboard View', style: CustomLabels.h1 ),

          SizedBox( height: 10 ),

          WhiteCard(
            title: 'Dashboard Page',
            child: Text( 'Hola Mundo!!' )
          )

        ],
      ),
    );
  }
}