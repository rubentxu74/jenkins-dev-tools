import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/datasources/plugins_datasource.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/ui/cards/white_card.dart';



class PluginsView extends StatefulWidget {

  bool isDefault;
  String title;

  PluginsView({required this.title, this.isDefault = false, });

  @override
  State<PluginsView> createState() => _PluginsViewState();
}

class _PluginsViewState extends State<PluginsView> {

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (widget.isDefault) {
        Provider.of<PluginsProvider>(context, listen: false).getDefaultPlugins();
      } else {
        Provider.of<PluginsProvider>(context, listen: false).getAllPlugins();
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PluginsProvider>(builder: (context, provider, child) {
      final size = MediaQuery.of(context).size;
      final isLoading = provider.isLoading;

      return Container(
        width: size.width,
        child: (isLoading)?
              WhiteCard(
                  child: Container(
                    alignment: Alignment.center,
                    height: 300,
                    child: CircularProgressIndicator(),
                  )
              ):_PluginsViewBody(provider, context, widget.title, widget.isDefault)

       );

    });

  }
}

Widget _PluginsViewBody(PluginsProvider provider, BuildContext context, String title, bool isDefault) {
  final plugins;
  if (isDefault) {
    plugins = provider.defaultPlugins;
  } else {
    plugins = provider.filteredPlugins.isEmpty ?
    provider.allPlugins :
    provider.filteredPlugins;
  }

  return Container(
    width: double.infinity,
    height: heightTable,
    child: PaginatedDataTable(
      header: Row(
        children: [
          Text(title, maxLines: 2),
          if(!isDefault)
            ...[
              Spacer(),
              Expanded(
              child: TextField(
                controller: provider.searchController,
                decoration: InputDecoration(
                  labelText: 'Search',
                  prefixIcon: Icon(Icons.search),
                ),
                onChanged: (value) {
                  final searchText = value.toLowerCase();
                  provider.filterData(searchText);
                },
              ),
            ),
          ]
        ],
      ),
      columns: [
        DataColumn(label: Text('Nombre')),
        DataColumn(label: Text('Versión')),
        if(!isDefault)
          DataColumn(label: Text('Acción')),
      ],
      source: PluginDataSource(plugins, context, isDefault: isDefault),
      rowsPerPage: 10,

    ),
  );
}
