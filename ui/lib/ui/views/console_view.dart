import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/services/services.dart';

class ConsoleView extends StatefulWidget {
  String initialMessage;
  String finalMessage;

  ConsoleView({required this.initialMessage, required this.finalMessage});

  @override
  State<ConsoleView> createState() => _ConsoleViewState();
}

class _ConsoleViewState extends State<ConsoleView>
    with SingleTickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) => _scrollToLast());
  }

  void _scrollToLast() {
    // if ScrollController not attached to any scroll views, return
    if (_scrollController.hasClients == false) return;
    final double maxScrollExtent = _scrollController.position.maxScrollExtent;

    _scrollController.animateTo(
      maxScrollExtent + 100,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Consumer<ConsoleProvider>(builder: (context, console, child) {
      _scrollToLast();

      return Scaffold(
        appBar: AppBar(
          title: console.isComplete
              ? Text(
                  '${widget.finalMessage} - Console Logs:  ${console.length}',
                  overflow: TextOverflow.ellipsis,
                )
              : Text(
                  '${widget.initialMessage} - Console Logs:  ${console.length}',
                  overflow: TextOverflow.ellipsis,
                ),

          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => NavigationService.replaceTo('dashboard'),
          ),
          // info limits logs
        ),
        body: Container(
          height: size.height - heightNavbar,
          width: size.width,
          color: Colors.black,
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              Container(
                height: size.height - heightNavbar - 115,
                width: size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 14.0),
                  child: ListView.builder(
                    controller: _scrollController,
                    padding: EdgeInsets.only(bottom: 30.0),
                    itemCount: console.length,
                    itemBuilder: (context, index) {
                      bool isLastIntem = false;
                      if (index >= console.length - 2) {
                        isLastIntem = true;
                      }
                      return RichText(
                        overflow: TextOverflow.ellipsis,
                        text: CreateTextSpan(
                            console.logs[index], console, context, isLastIntem),
                      );
                    },
                  ),
                ),
              ),
              (!console.isComplete)
                  ? Center(
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    )
                  : Center(
                      child: TextButton.icon(
                        onPressed: () =>
                            NavigationService.replaceTo('dashboard'),
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                        label: Text(
                          'Go to Dashboard',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
            ],
          ),
        ),
      );
    });
  }
}


TextSpan CreateTextSpan(String log, ConsoleProvider console, BuildContext context, bool isLastItem) {
  Color textColor = resolveTextColor(log);

  return TextSpan(
      text: resolveMessage(log), style: TextStyle(color: textColor));
}

String resolveMessage(String message) {
  try {
    Map<String, dynamic> messageMap = jsonDecode(message);
    String event = messageMap["event"];
    dynamic payload = messageMap["payload"];
    String payloadString = payload is Map ? payload.toString() : payload;
    return "Event: '$event', --> $payloadString";
  } catch (e) {
    return message;
  }
}

Color resolveTextColor(String log) {
  Color textColor = Colors.white;
  if (log.contains("Error")) {
    textColor = Colors.red;
  } else if (log.contains("Success") ||
      log.startsWith('Step') ||
      log.contains('Successfull') ||
      log.contains('DoneScript')) {
    textColor = Colors.green;
  } else if (log.contains("Warn")) {
    textColor = Colors.yellow;
  } else if (log.contains("Info")) {
    textColor = Colors.blue;
  } else if (log.contains("Message")) {
    textColor = Colors.purple;
  } else if (log.startsWith('--->')) {
    textColor = Colors.cyan;
  }
  return textColor;
}
