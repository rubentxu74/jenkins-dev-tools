import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/datasources/asdf_tools.dart';
import 'package:ui/datasources/plugins_datasource.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/asdf_provider.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/ui/cards/white_card.dart';


class AsdfView extends StatefulWidget {

  bool isDefault;
  String title;

  AsdfView({required this.title, this.isDefault = false, });

  @override
  State<AsdfView> createState() => _AsdfViewState();
}

class _AsdfViewState extends State<AsdfView> {

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (widget.isDefault) {
        Provider.of<AsdfProvider>(context, listen: false).getDefaultTools();
      } else {
        Provider.of<AsdfProvider>(context, listen: false).getAvailableTools();
      }

    });
  }


  @override
  Widget build(BuildContext context) {
    return Consumer<AsdfProvider>(builder: (context, provider, child) {
      final size = MediaQuery.of(context).size;
      final isLoading = provider.isLoading;

      return Container(
        width: size.width,
        child: (isLoading)?
              WhiteCard(
                  child: Container(
                    alignment: Alignment.center,
                    height: 300,
                    child: CircularProgressIndicator(),
                  )
              ):_Body(provider, context, widget.title, widget.isDefault)

       );

    });

  }
}

Widget _Body(AsdfProvider provider, BuildContext context, String title, bool isDefault) {
  final plugins;
  if (isDefault) {
    plugins = provider.defaultAsdfTools;
  } else {
    plugins = provider.filteredAsdfTools.isEmpty ?
    provider.allAsdfTools :
    provider.filteredAsdfTools;
  }

  return Container(
    width: double.infinity,
    height: heightTable,
    child: PaginatedDataTable(
      header: Row(
        children: [
          Text(title, maxLines: 2),
          if(!isDefault)
            ...[
              Spacer(),
              Expanded(
              child: TextField(
                controller: provider.searchController,
                decoration: InputDecoration(
                  labelText: 'Search',
                  prefixIcon: Icon(Icons.search),
                ),
                onChanged: (value) {
                  final searchText = value.toLowerCase();
                  provider.filterData(searchText);
                },
              ),
            ),
          ]
        ],
      ),
      columns: [
        DataColumn(label: Text('Nombre')),
        DataColumn(label: Text('Versión')),
        if(!isDefault)
          DataColumn(label: Text('Acción')),
      ],
      source: AsdfDataSource(plugins, context, isDefault: isDefault),
      rowsPerPage: 10,

    ),
  );
}
