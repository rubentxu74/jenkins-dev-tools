import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/api/websocket_client.dart';
import 'package:ui/global.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/router/router.dart';
import 'package:ui/ui/shared/shared.dart';

class SetupImageView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Consumer<SetupJenkins>(builder: (context, setup, child) {
      return ListView(
        physics: ClampingScrollPhysics(),
        children: [
          Container(
            color: Colors.transparent,
            margin: EdgeInsets.only(
              left: 10,
            ),
            width: size.width,
            height: size.height - heightNavbar,
            child: Stepper(
              type: size.width > 700
                  ? StepperType.horizontal
                  : StepperType.vertical,
              physics: ScrollPhysics(),
              currentStep: setup.currentStep,
              onStepTapped: (step) => {
                setup.currentStep = step,
              },
              onStepContinue: () => continued(setup, context),
              onStepCancel: () => cancel(setup),
              steps: <Step>[
                _buildStepOne(setup),
                _buildStepTwo(setup),
                _buildStepThree(setup),
              ],
              controlsBuilder: (BuildContext context, ControlsDetails details) {
                final _isLastStep = setup.currentStep == 3 - 1;
                return Container(
                    margin: const EdgeInsets.only(top: 50),
                    child: Row(children: [
                      if (setup.currentStep != 0)
                        Container(
                            child: ElevatedButton(
                                child: Text('Atrás'),
                                onPressed: details.onStepCancel)),
                      const SizedBox(
                        width: 12,
                      ),
                      Container(
                          child: ElevatedButton(
                              child: Text(_isLastStep ? 'Build' : 'Continuar'),
                              onPressed: setup.createdImage
                                  ? null
                                  : details.onStepContinue)),
                    ]));
              },
            ),
          ),
        ],
      );
    });
  }

  Step _buildStepThree(SetupJenkins setup) {
    return Step(
      title: new Text('Extras imágen'),
      content: Container(
        child: Column(
          children: [
            CheckboxListTile(
              title: Text('Instalar Podman'),
              value: setup.installPodmanIsChecked,
              onChanged: (value) {
                setup.installPodmanIsChecked = value!;
                print(setup.installPodmanIsChecked);
              },
            ),
            CheckboxListTile(
              title: Text('Instalar Python'),
              value: setup.installPythonIsChecked,
              onChanged: (value) {
                setup.installPythonIsChecked = value!;
              },
            ),
          ],
        ),
      ),
      isActive: setup.currentStep >= 0,
      state: setup.currentStep >= 2 ? StepState.complete : StepState.disabled,
    );
  }

  Step _buildStepTwo(SetupJenkins setup) {
    return Step(
      title: new Text(
        'Versión de Jenkins',
        overflow: TextOverflow.ellipsis,
      ),
      content: Container(
        width: 300,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Selecciona la versión de Jenkins que deseas instalar.',
                overflow: TextOverflow.ellipsis),
            DropdownButton(
              isExpanded: false,
              value: setup.jenkinsVersion,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              items: setup.jenkinsVersionList.map((String version) {
                return DropdownMenuItem(
                  value: version,
                  child: Text(version),
                );
              }).toList(),
              onChanged: (value) {
                print(value);
                setup.jenkinsVersion = value!;
                print(setup.jenkinsVersion);
              },
            ),
          ],
        ),
      ),
      isActive: setup.currentStep >= 0,
      state: setup.currentStep >= 1 ? StepState.complete : StepState.disabled,
    );
  }

  Step _buildStepOne(SetupJenkins setup) {
    return Step(
      title: new Text('Empezamos con la configuración',
          overflow: TextOverflow.ellipsis),
      content: Container(
        // Long description
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
                'Necesitamos que nos proporciones algunos datos para poder crear la imagen de contenedor de Jenkins.',
                overflow: TextOverflow.ellipsis),
            SizedBox(height: 20.0),
            UnnorderedListItem(
                title: TextSpan(
              text:
                  'Crearemos incialmente una imagen de Jenkins con la versión que nos indiques.',
              style: TextStyle(color: Colors.black),
            )),
            UnnorderedListItem(
                title: TextSpan(
              text:
                  'Luego te daremos la opción de instalar algunos extras como Podman, Python, dentro del la imágen.',
              style: TextStyle(color: Colors.black),
            )),
            UnnorderedListItem(
                title: TextSpan(
              text:
                  'Por último, construiremos la imágen y la ejecutaremos en un contenedor.',
              style: TextStyle(color: Colors.black),
            )),
            SizedBox(height: 20.0),
            Text(
                'Pasa a la siguiente etapa para seleccionar la versión de Jenkins que deseas instalar.',
                overflow: TextOverflow.ellipsis),
          ],
        ),
      ),
      isActive: setup.currentStep >= 0,
      state: setup.currentStep >= 0 ? StepState.complete : StepState.disabled,
    );
  }

  switchStepsType(SetupJenkins setup) {
    setup.stepperType == StepperType.vertical
        ? setup.stepperType = StepperType.horizontal
        : setup.stepperType = StepperType.vertical;
  }

  continued(SetupJenkins setup, BuildContext context) {
    setup.currentStep < 2
        ? setup.currentStep += 1
        : navigateToConsole(setup, context);
  }

  cancel(SetupJenkins setup) {
    setup.currentStep > 0 ? setup.currentStep -= 1 : null;
  }

  void navigateToConsole(SetupJenkins setup, BuildContext context) async {
    if (setup.createdImage) return;
    Navigator.pushNamed(context, AppRouter.consoleImageRoute);


    SocketRequest request = SocketRequest(
        type: RpcType.Action,
        name: 'CreateJenkinsImage',
        payload: {
          "id": setup.jenkinsVersion,
          "installPodman": setup.installPodmanIsChecked,
          "installPython": setup.installPythonIsChecked,
          "noCache": true
        });
    var console = Provider.of<ConsoleProvider>(context, listen: false);
    console.reset();
    bool result = await console.executeConsoleAction(request);

    if (result) {
      var status = Provider.of<ApplicationStatus>(context, listen: false);

      status
        ..currentJenkinsVersion = setup.jenkinsVersion
        ..installedPodman = setup.installPodmanIsChecked
        ..installedPython = setup.installPythonIsChecked;
      console.isComplete = true;
      setup.createdImage = true;
      print("Change status to Image created successfully");
    }
    console.isComplete = true;
    print( "Complete: ${console.isComplete}");
    setup.currentStep = 0;
  }
}
