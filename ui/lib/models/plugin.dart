

class Plugin {
  final String name;
  final String shortName;
  final String excerpt;
  String version;
  bool isSelected;
  bool isInstalled;

  Plugin({ required this.name, required this.version, this.excerpt= '', this.isSelected = false,  this.shortName = '', this.isInstalled = false});

  String toString() {
    return 'Plugin: $name, $version, $excerpt, $isSelected, $isInstalled';
  }

  factory Plugin.fromJson(Map<String, dynamic> json) {
    return Plugin(
      name: json['name'],
      version: json['version'],
      excerpt: json['excerpt']??'',
      isSelected: json['isSelected']??false,
      isInstalled: false,
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'version': version,
    'excerpt': excerpt,
    'isSelected': isSelected,
    'isInstalled': isInstalled,
  };


}
