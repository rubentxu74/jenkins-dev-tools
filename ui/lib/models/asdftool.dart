class AsdfTool {
  String name;
  String version;
  String url;
  bool isSelected;

  AsdfTool({required this.name, required this.url, this.version = 'latest', this.isSelected = false});

  factory AsdfTool.fromJson(Map<String, dynamic> json) {
    return AsdfTool(
      name: json['name'],
      version: json['version']??"latest",
      url: json['url'],
      isSelected: json['isSelected'] ?? false,
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'version': version,
    'url': url,
    'isSelected': isSelected,
  };
}
