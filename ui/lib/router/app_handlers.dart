
import 'package:fluro/fluro.dart';
import 'package:ui/providers/plugins_provider.dart';
import 'package:ui/ui/views/views.dart';

import '../api/api.dart';

class AppHandlers {

  static Handler status = Handler(
    handlerFunc: ( context, params ) {
      return StatusView(rpcApi: WebsocketRPCApi());
    }
  );


  static Handler setupImage = Handler(
    handlerFunc: ( context, params ) {
       return SetupImageView();
    }
  );

  static Handler setupContainer = Handler(
      handlerFunc: ( context, params ) {
        return SetupContainerView();
      }
  );

  static Handler icons = Handler(
      handlerFunc: ( context, params ) {
        return IconsView();
      }
  );

  static Handler consoleCreateImage = Handler(
      handlerFunc: ( context, params ) {
        return ConsoleView(
            initialMessage: "Iniciando construcción de Imagen de Jenkins",
            finalMessage: "Finalizada construcción de Imagen de Jenkins",
        );
      }
  );

  static Handler consoleJenkins = Handler(
      handlerFunc: ( context, params ) {
        return ConsoleView(
            initialMessage: "Iniciando construcción de Imagen de Jenkins",
            finalMessage: "Finalizada construcción de Imagen de Jenkins",
        );
      }
  );

  static Handler plugins(PluginsProvider pluginsProvider) {
    return Handler(
        handlerFunc: ( context, params ) {
          return PluginsView(title: "Plugins Disponibles",);
        }
    );
  }

}

