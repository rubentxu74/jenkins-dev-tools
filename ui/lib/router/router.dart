
import 'package:fluro/fluro.dart';
import 'package:ui/providers/plugins_provider.dart';

import 'app_handlers.dart';
import 'no_page_found_handlers.dart';


class AppRouter {

  static late FluroRouter router;

  static String rootRoute     = '/';

  // Console Router
  static String consoleImageRoute = '/console/createImage';
  static String consoleJenkinsRoute = '/console/jenkins';

  // Dashboard
  static String dashboardRoute  = '/dashboard';
  static String setupImageRoute      = '/dashboard/setup/image';
  static String setupContainerRoute      = '/dashboard/setup/container';
  static String iconsRoute      = '/dashboard/icons';
  static String blankRoute      = '/dashboard/blank';
  static String pluginsRoute    = '/dashboard/plugins';



  static void configureRoutes(PluginsProvider pluginsProvider) {    // root Routes
    router = new FluroRouter();
    router.define( rootRoute, handler: AppHandlers.status, transitionType: TransitionType.inFromTop );
    router.define( consoleImageRoute, handler: AppHandlers.consoleCreateImage, transitionType: TransitionType.material );
    router.define( consoleJenkinsRoute, handler: AppHandlers.consoleJenkins, transitionType: TransitionType.material );

    // Dashboard
    router.define( dashboardRoute, handler: AppHandlers.status, transitionType: TransitionType.material );
    router.define( setupImageRoute, handler: AppHandlers.setupImage, transitionType: TransitionType.material );
    router.define( setupContainerRoute, handler: AppHandlers.setupContainer, transitionType: TransitionType.material );
    router.define( iconsRoute, handler: AppHandlers.icons, transitionType: TransitionType.material );
    router.define( pluginsRoute, handler: AppHandlers.plugins(pluginsProvider), transitionType: TransitionType.material );

    // 404
    router.notFoundHandler = NoPageFoundHandlers.noPageFound;

  }
  


}

