import 'package:provider/provider.dart';
import 'package:fluro/fluro.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/ui/views/no_page_found_view.dart';


class NoPageFoundHandlers {
  static Handler noPageFound = Handler(
    handlerFunc: ( context, params ) {
      Provider.of<SideMenuProvider>(context!, listen: false).setCurrentPageUrl('/404');
      return NoPageFoundView();
    }
  );


}

