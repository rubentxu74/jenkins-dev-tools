import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ui/api/api.dart';
import 'package:ui/providers/asdf_provider.dart';
import 'package:ui/providers/providers.dart';
import 'package:ui/router/router.dart';
import 'package:ui/services/services.dart';
import 'package:ui/ui/layouts/layouts.dart';

import 'global.dart';

void main() async {
  await LocalStorage.configurePrefs();
  runApp(AppState());
}

class AppState extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    WebsocketRPCApi rpcClient = WebsocketRPCApi();
    PluginsService pluginsService = PluginsService();
    AsdfToolService asdfToolService = AsdfToolService();
    PluginsProvider pluginsProvider = PluginsProvider(pluginsService: pluginsService);
    AppRouter.configureRoutes(pluginsProvider);

    return MultiProvider(providers: [
      ChangeNotifierProvider(lazy: false, create: (_) => SideMenuProvider()),
      // ChangeNotifierProvider(create: ( _ ) => CategoriesProvider() ),
      FutureProvider<Configuration>(
          create: (context) => Configuration.load(context),
          initialData: Configuration({})),
      ChangeNotifierProvider<SocketProvider>(
          create: (context) => SocketProvider()),
      ChangeNotifierProvider<ConsoleProvider>(
          create: (_) => ConsoleProvider(limitLogs: 240, client: rpcClient)),
      ChangeNotifierProvider<SetupJenkins>(create: (_) => SetupJenkins()),
      ChangeNotifierProvider<ApplicationStatus>(create: (_) => ApplicationStatus()),
      ChangeNotifierProvider<PluginsProvider>(create: (_) => pluginsProvider),
      ChangeNotifierProvider<AsdfProvider>(create: (_) => AsdfProvider(asdfToolService: asdfToolService)),
    ], child: _my_app());
  }
}

Widget _my_app() {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    title: globalTitle,
    initialRoute: '/',
    onGenerateRoute: AppRouter.router.generator,
    navigatorKey: NavigationService.navigatorKey,
    scaffoldMessengerKey: NotificationsService.messengerKey,
    theme: ThemeData.light().copyWith(
      shadowColor: Colors.transparent,
      canvasColor: Color( 0xffEDF1F2 ),
        scrollbarTheme: ScrollbarThemeData().copyWith(
            thumbColor:
                MaterialStateProperty.all(Colors.grey.withOpacity(0.9))),
        colorScheme: ColorScheme.light(

            primary: Colors.blueGrey,
            secondary: Colors.orange,
            onPrimary: Colors.white,
            onSecondary: Colors.white,
            onSurface: Colors.black,
            onBackground: Colors.black,
            onError: Colors.red,
            background: Colors.white,
            surface: Colors.white,
            error: Colors.red)),
    builder: (_, child) {
      return DashboardLayout(child: child!);
    },
  );
}
