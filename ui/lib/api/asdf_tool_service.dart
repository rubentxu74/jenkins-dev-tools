import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:ui/models/models.dart';

class AsdfToolService {
  Future<List<AsdfTool>> getTools() async {
    // Cargar el archivo JSON desde el directorio assets de la aplicación
    final jsonString = await rootBundle.loadString('assets/services/tools.json');

    // Decodificar la cadena JSON en una lista de objetos AsdfTool
    final List<dynamic> jsonList = json.decode(jsonString);
    final List<AsdfTool> tools = jsonList.map((json) => AsdfTool(
      name: json['name'],
      version: json['version'],
      url: json['url'],
      isSelected: json['isSelected'] ?? false,
    )).toList();

    return tools;
  }

  Future<List<AsdfTool>> defaultTools() async {
    // Cargar el archivo JSON desde el directorio assets de la aplicación
    final jsonString = await rootBundle.loadString('assets/services/default_tools.json');

    // Decodificar la cadena JSON en una lista de objetos AsdfTool
    final List<dynamic> jsonList = json.decode(jsonString);
    final List<AsdfTool> tools = jsonList.map((json) => AsdfTool(
      name: json['name'],
      version: json['version']??'latest',
      url: json['url'],
      isSelected: json['isSelected'] ?? false,
    )).toList();

    return tools;
  }
}