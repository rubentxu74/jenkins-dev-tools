import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:ui/api/websocket_client.dart';
import 'package:ui/models/models.dart';

class PluginsService {
  WebsocketRPCApi _socketClient;

  PluginsService()
      : _socketClient = WebsocketRPCApi() {}

  Future<List<Plugin>> getAllPlugins() async {
    final completer = Completer<List<Plugin>>();

    List<Plugin> plugins = [];
    SocketRequest request = SocketRequest(
        type: RpcType.Action,
        name: 'GetPaginatedAvailablePlugins',
        payload: {'page': 1, 'pageSize': 50});
    _socketClient.connectRequestResponse(request, (message) {
      var decodeJson = json.decode(message);

      for (var plugin in decodeJson['body']['plugins']) {
        var entity = Plugin(
            name: plugin['name'],
            shortName: plugin['name'],
            version: plugin['version'],
            excerpt: plugin['excerpt'],
            isSelected: false,
            isInstalled: false);
        plugins.add(entity);
      }
      print("All Plugins size: ${plugins.length})}");
    }, onError: (error) {
      print("Error: $error");
      completer.completeError(error);
    }, onDone: () {
      print("Ondone called");
      completer.complete(plugins);
    }, cancelOnError: true);

    return completer.future;
  }

  Future<List<Plugin>> getInstalledPlugins() async {
    final completer = Completer<List<Plugin>>();

    List<Plugin> plugins = [];
    SocketRequest request = SocketRequest(
        type: RpcType.Action,
        name: 'GetInstalledPlugins',
        payload: {'page': 1, 'pageSize': 50});
    _socketClient.connectRequestResponse(request, (message) {
      plugins = (json.decode(message) as List)
          .map((plugin) => Plugin.fromJson(plugin))
          .toList();
      print("Installed Plugins: $plugins");
    }, onError: (error) {
      completer.completeError(error);
    }, onDone: () {
      print("Ondone called");
      completer.complete(plugins);
    }, cancelOnError: true);

    return completer.future;
  }

  Future<List<Plugin>> getDefaultPlugins() async {
    final jsonString = await rootBundle.loadString('assets/services/default_plugins.json');
    return (json.decode(jsonString) as List)
        .map((plugin) => Plugin.fromJson(plugin))
        .toList();
  }
}
