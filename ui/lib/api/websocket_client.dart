import 'dart:async';
import 'dart:convert';

import 'package:ui/global.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class WebsocketRPCApi {
  WebsocketRPCApi();

  void connect(void onData(dynamic event),
      {Function? onError, void onDone()?, bool? cancelOnError}) {
    String url = "${endPointRpc}/action?client_id=12345'";
    Uri uri = Uri.parse(url);
    print('connecting websocket to ${uri.toString()}');
    WebSocketChannel channel = WebSocketChannel.connect(uri);
    try {
      channel.stream.listen(onData as void Function(dynamic event)?,
          onError: onError, onDone: onDone, cancelOnError: cancelOnError);
    } catch (e) {
      print('Error Building Container: $e');
      channel.sink.done;
      channel.sink.close();
    }
  }

  void connectRequestResponse(SocketRequest request, void onData(dynamic event),
      {required Function onError,
      required void onDone(),
      bool? cancelOnError}) {
    String url =
        "${endPointRpc}/${request.type.name.toLowerCase()}?client_id=12345'";
    Uri uri = Uri.parse(url);
    print('connecting websocket to ${uri.toString()}');

    WebSocketChannel channel = WebSocketChannel.connect(uri);
    try {
      channel.stream.listen(onData as void Function(dynamic event)?,
          onError: onError, onDone: onDone, cancelOnError: cancelOnError);

      channel.sink.add(request.serializeToJson());
      print('Request: ${request.serializeToJson()}');
    } catch (e) {
      print('Error Building Container: $e');
      channel.sink.done;
      channel.sink.close();
    }
  }

  Future<bool> executeAction(SocketRequest request, void onData(dynamic event),
      {required Function errorFunc}) async {
    Completer<bool> completer = Completer<bool>();

    try {
      connectRequestResponse(request, onData,
          onError: (e) {
            errorFunc(e);
            completer.complete(false);
          },
          onDone: () => completer.complete(true),
          cancelOnError: true);
    } catch (e) {
      errorFunc(e);
    }
    return completer.future;
  }

  Future<bool> installAsdfTool(void onData(dynamic event),
      {required Function errorFunc}) async {
    SocketRequest request =
        SocketRequest(type: RpcType.Script, script: 'InstallAsdf', payload: {
      "asdf": {"version": "v0.10.2"}
    });
    try {
      return await executeAction(request, onData, errorFunc: errorFunc);
    } catch (ex) {
      errorFunc(ex);
    }
    return false;
  }

  Future<bool> installJenkinsPlugins(
      List<String> plugins, void onData(dynamic event),
      {required Function errorFunc}) async {
    SocketRequest request =
        SocketRequest(type: RpcType.Script, script: 'InstallPlugins', payload: {
      "plugins": plugins,
    });
    try {
      return await executeAction(request, onData, errorFunc: errorFunc);
    } catch (ex) {
      errorFunc(ex);
    }
    return false;
  }

  Future<bool> runJenkinsContainer(String jenkinsVersion,
      String libraryDirectory, void onData(dynamic event),
      {required Function errorFunc}) async {
    SocketRequest request = SocketRequest(
        type: RpcType.Action,
        name: 'RunJenkinsContainer',
        payload: {
          "containerName": "jenkins-dev-tool",
          "imageName": "jenkins-dev-container",
          "tag": jenkinsVersion,
          "pathRepositories": libraryDirectory,
          "hostPort": "8080",
          "envs": [
            "JAVA_OPTS=-Djenkins.install.runSetupWizard=false -Dhudson.security.csrf.GlobalCrumbIssuerConfiguration.DISABLE_CSRF_PROTECTION=true -Dhudson.plugins.git.GitSCM.ALLOW_LOCAL_CHECKOUT=true"
          ]
        });

    try {
      return await executeAction(request, onData, errorFunc: errorFunc);
    } catch (ex) {
      errorFunc(ex);
    }
    return false;
  }

  Future<bool> healthCheckRemoteScriptInJenkins(void onData(dynamic event),
      {required Function errorFunc}) async {
    SocketRequest request =
        SocketRequest(type: RpcType.Script, script: 'HealthCheck', payload: {});

    try {
      return await executeAction(request, onData, errorFunc: errorFunc);
    } catch (ex) {
      errorFunc(ex);
    }
    return false;
  }


  stopJenkinsContainer(void Function(dynamic event) param0,
      {required void Function(dynamic e) errorFunc}) {
    SocketRequest request = SocketRequest(
        type: RpcType.Action,
        name: 'StopJenkinsContainer',
        payload: {
          "containerName": "jenkins-dev-tool",
        });

    try {
      return executeAction(request, param0, errorFunc: errorFunc);
    } catch (ex) {
      errorFunc(ex);
    }
  }
}

enum RpcType { Action, Script }

class SocketRequest {
  final RpcType type;
  final String? name;
  final String? script;
  final Map? payload;

  SocketRequest(
      {required this.type,
      this.payload,
      this.name = "N/A",
      this.script = "N/A"});

  Map<String, dynamic> toMap() => {
        'type': type.name,
        'name': name,
        'script': script,
        'payload': payload,
      };

  String serializeToJson() {
    return jsonEncode(toMap());
  }
}
