const String globalTitle = "Jenkins Container Dev";
const double limitDesktop = 850;
const double heightNavbar = 50;
const double widthSidebar = 250;
const double heightTable = 665;
const String endPointRpc = "ws://localhost:8081/ws/rpc";
