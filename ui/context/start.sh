#!/bin/sh

# Create the sock file
#nc -lU /run/guest-services/backend.sock &
#/service -socket /run/guest-services/backend.sock
## si el proceso anterior falla, se aborta el script con un mensaje de error
#if [ $? -ne 0 ]; then
#    echo "Error: service failed to start"
#fi

# Create the volume
mkdir -p /run/guest-services

sed -i 's/listen  .*/listen 32433;/g' /etc/nginx/conf.d/default.conf


# Start the main process sss
exec /service -socket /run/guest-services/backend.sock & nginx -g 'daemon off;'
