package templates

import (
	"bytes"
	"fmt"
	"rubentxu.dev/jenkins_dev_container/cdi"
	"text/template"
)

const TemplateFactoryName = "TemplateFactory"

// TemplateFactory is a struct that holds a map of templates and a base path.
type TemplateFactory struct {
	templates map[string]*template.Template
	basePath  string
}

// NewTemplateFactory creates a new instance of the TemplateFactory struct, and returns a pointer to it.
func NewTemplateFactory(basePath string) *TemplateFactory {
	return &TemplateFactory{
		templates: make(map[string]*template.Template),
		basePath:  basePath,
	}
}

func GetTemplateFactory() *TemplateFactory {
	service, ok := cdi.GetServiceLocator().GetService(TemplateFactoryName)
	if !ok {
		panic("Template Factory not found")
	}
	return service.(*TemplateFactory)
}

func (f *TemplateFactory) GetKey() string {
	return "templateFactory"
}

// AddTemplate adds a template to the TemplateFactory's map of templates.
func (f *TemplateFactory) AddTemplate(name, text string) error {
	tmpl, err := template.New(name).Parse(text)
	if err != nil {
		return err
	}
	f.templates[name] = tmpl
	return nil
}

// AddTemplatesFromFile adds a template to the TemplateFactory's map of templates.
func (f *TemplateFactory) AddTemplatesFromFile(name, templateBase string, templateScript string) error {
	base, err := template.ParseFiles(fmt.Sprintf("%s/%s", f.basePath, templateBase), fmt.Sprintf("%s/%s", f.basePath, templateScript))
	if err != nil {
		fmt.Printf("Error parsing template %s: %v\n", name, err)
		return err
	}
	tmpl := template.Must(base, nil)
	f.templates[name] = tmpl
	fmt.Printf("Template %s added\n", name)
	return nil
}

// AddTemplateFromFile adds a template to the TemplateFactory's map of templates.
func (f *TemplateFactory) AddTemplateFromFile(name, subPath string) error {
	tmpl, err := template.ParseFiles(fmt.Sprintf("%s/%s", f.basePath, subPath))
	if err != nil {
		fmt.Printf("Error parsing template %s: %v\n", name, err)
		return err
	}
	f.templates[name] = tmpl
	fmt.Printf("Template %s added\n", name)
	return nil
}

// TemplateScript is a struct that holds the content of a template.
type TemplateScript struct {
	content string
}

// RenderTemplate renders a template and returns a buffer.
func (t *TemplateFactory) RenderTemplate(name string, data interface{}) (*bytes.Buffer, error) {
	tmpl, ok := t.templates[name]
	buf := bytes.NewBuffer([]byte{})
	if !ok {
		return buf, fmt.Errorf("Template not found: %s", name)
	}

	if err := tmpl.Execute(buf, data); err != nil {
		fmt.Printf("\nRender Error: %v\n", err)
		return buf, err
	}
	if name == "Dockerfile" {
		println("Rendered template content: " + buf.String())
	}
	return buf, nil
}
