package jenkins

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/websocket/v2"
	"io/ioutil"
	"net/http"
	"os"
	"rubentxu.dev/jenkins_dev_container/cdi"
)

const JenkinsServiceName = "jenkinsService"

type WebSocketClient struct {
	Conn *websocket.Conn
}

type JenkinsServices struct {
	BaseURL string
	Auth    *JenkinsAuth
	Crumb   string
	Clients []*WebSocketClient
}

type Job struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Build struct {
	Number   int    `json:"number"`
	URL      string `json:"url"`
	Building bool   `json:"building"`
	Result   string `json:"result"`
}

type JenkinsAuth struct {
	Username string
	Token    string
}

type RemoteScript struct {
	Script string `json:"script"`
}

func NewJenkinsServices() *JenkinsServices {
	url := os.Getenv("JENKINS_URL")
	username := os.Getenv("JENKINS_USERNAME")
	pasword := os.Getenv("JENKINS_PASSWORD")
	println("Jenkins URL: %s", url)
	println("Jenkins Username: %s", username)

	return &JenkinsServices{
		BaseURL: url,
		Auth: &JenkinsAuth{
			Username: username,
			Token:    pasword,
		},
	}
}

func GetJenkinsService() *JenkinsServices {
	service, ok := cdi.GetServiceLocator().GetService(JenkinsServiceName)
	if !ok {
		panic("Jenkins Service not found")
	}
	return service.(*JenkinsServices)
}

func (s *JenkinsServices) GetKey() string {
	return "jenkinsServices"
}

func (s *JenkinsServices) AddClient(conn *websocket.Conn) {
	s.Clients = append(s.Clients, &WebSocketClient{Conn: conn})
}

func (s *JenkinsServices) RemoveClient(conn *websocket.Conn) {
	for i, client := range s.Clients {
		if client.Conn == conn {
			s.Clients = append(s.Clients[:i], s.Clients[i+1:]...)
		}
	}
}

func (s *JenkinsServices) TriggerBuild(jobName string) ([]byte, error) {
	url := fmt.Sprintf("%s/job/%s/build", s.BaseURL, jobName)
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func (s *JenkinsServices) GetJob(jobName string) (map[string]interface{}, error) {
	url := fmt.Sprintf("%s/job/%s/api/json", s.BaseURL, jobName)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (s *JenkinsServices) GetJobs() ([]Job, error) {
	url := fmt.Sprintf("%s/api/json?tree=jobs[name,url]", s.BaseURL)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data struct {
		Jobs []Job `json:"jobs"`
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	return data.Jobs, nil
}

func (s *JenkinsServices) GetBuild(jobName string, buildNumber int) (map[string]interface{}, error) {
	url := fmt.Sprintf("%s/job/%s/%d/api/json", s.BaseURL, jobName, buildNumber)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (s *JenkinsServices) GetBuildConsoleOutput(jobName string, buildNumber int) (string, error) {
	url := fmt.Sprintf("%s/job/%s/%d/consoleText", s.BaseURL, jobName, buildNumber)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func (s *JenkinsServices) GetBuildConsoleOutputHTML(jobName string, buildNumber int) (string, error) {
	url := fmt.Sprintf("%s/job/%s/%d/console", s.BaseURL, jobName, buildNumber)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func (s *JenkinsServices) GetCrumb() (string, error) {
	url := fmt.Sprintf("%s/crumbIssuer/api/json", s.BaseURL)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var data struct {
		Crumb string `json:"crumb"`
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return "", err
	}

	return data.Crumb, nil
}
