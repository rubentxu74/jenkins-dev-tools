package jenkins

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"rubentxu.dev/jenkins_dev_container/usecase/templates"
	"time"
)

func (j *JenkinsServices) ExecuteGlobalRemoteScript(script string, maxRetries int, data interface{}) ([]byte, error) {
	templateFactory := templates.GetTemplateFactory()

	body, err := templateFactory.RenderTemplate(script, data)
	if err != nil {
		return nil, err
	}

	formBody := bytes.NewBuffer([]byte("script="))
	formBody.WriteString(url.QueryEscape(body.String()))
	client := &http.Client{}

	retryInterval := 5 * time.Second

	for retries := 0; retries < maxRetries; retries++ {
		req, err := http.NewRequest("POST", j.BaseURL+"/scriptText", formBody)
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

		if err != nil {
			if retries >= maxRetries {
				return nil, err
			} else {
				time.Sleep(retryInterval)
				continue
			}
		}

		req.SetBasicAuth(j.Auth.Username, j.Auth.Token)

		resp, err := client.Do(req)
		if err != nil {
			if retries >= maxRetries {
				return nil, err
			} else {
				time.Sleep(retryInterval)
				continue
			}
		}
		defer resp.Body.Close()

		response, err := io.ReadAll(resp.Body)
		if err != nil {
			if retries >= maxRetries {
				return nil, err
			} else {
				time.Sleep(retryInterval)
				continue
			}
		}

		if resp.StatusCode == 200 {
			// Si la respuesta es JSON válido, devuelve la respuesta
			fmt.Printf("Cuerpo de respuesta: %s\n", string(response))
			return response, nil
		}

		fmt.Printf("Código de estado: %d\n", resp.StatusCode)

		// Si la respuesta no es JSON válido, espera y vuelve a intentarlo
		time.Sleep(retryInterval)
		fmt.Println("Se vuelve a reintentar la llamada a Jenkins")
	}

	return nil, errors.New("Se agotaron los reintentos y la respuesta sigue siendo no válida")
}
