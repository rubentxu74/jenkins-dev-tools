package jenkins

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	jenkinsUpdateCenterURL = "https://updates.jenkins.io/current/update-center.actual.json"
)

type Plugin struct {
	Name         string             `json:"name"`
	Version      string             `json:"version"`
	Excerpt      string             `json:"excerpt"`
	URL          string             `json:"url"`
	Dependencies []PluginDependency `json:"dependencies"`
}

type PluginDependency struct {
	Name     string `json:"name"`
	Version  string `json:"version"`
	Optional bool   `json:"optional"`
}

func (j *JenkinsServices) GetPlugins() ([]Plugin, error) {
	// construir URL de la consulta con los parámetros de paginación
	resp, err := http.Get(jenkinsUpdateCenterURL)
	fmt.Println("Response from URL: ", jenkinsUpdateCenterURL)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil, fmt.Errorf("error haciendo solicitud HTTP GET: %v", err)
	}
	defer resp.Body.Close()

	// Decodificar la respuesta JSON
	var updateCenter struct {
		Plugins map[string]Plugin `json:"plugins"`
	}
	err = json.NewDecoder(resp.Body).Decode(&updateCenter)
	if err != nil {
		fmt.Println("Error 2: ", err)
		return nil, fmt.Errorf("error decodificando la respuesta JSON: %v", err)
	}

	// Crear una lista de plugins a partir del mapa de plugins
	var pluginList []Plugin
	for _, plugin := range updateCenter.Plugins {
		pluginList = append(pluginList, plugin)
	}

	return pluginList, nil
}
