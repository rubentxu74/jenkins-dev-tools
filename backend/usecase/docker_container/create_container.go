package docker_container

import (
	"archive/tar"
	"bytes"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/events"
	"golang.org/x/net/context"
	"io"
)

type DockerfileDataToCreateJenkinsImage struct {
	ImageBase     string
	AsdfDir       string
	AsdfDataDir   string
	Shell         string
	Volume        string
	JenkinsTools  string
	InstallPodman bool
	InstallPython bool
	NoCache       bool
}

// CreateImage creates a new image based on the given DockerfileDataToCreateJenkinsImage data and returns the image ID and an error if one occurs during the process.
func (s *DockerService) CreateImage(ctx context.Context, imageName string, dockerfileData *DockerfileDataToCreateJenkinsImage) (io.ReadCloser, error) {

	// Build the image using the generated Dockerfile contents
	buildCtx := s.createBuildContext(dockerfileData)
	buildOptions := types.ImageBuildOptions{
		Tags:       []string{imageName},
		Dockerfile: "Dockerfile",
		Context:    buildCtx,
		Remove:     true,
		PullParent: true,
		NoCache:    dockerfileData.NoCache,
	}

	println("Building image")
	resp, err := s.Client.ImageBuild(ctx, buildCtx, buildOptions)
	if err != nil {
		return nil, err
	}
	println("Image build")

	return resp.Body, nil

}

// GetImageEvents returns the events of the images
// It returns an error if one occurs
func (s *DockerService) GetImageEvents(ctx context.Context) error {

	eventCh, errCh := s.Client.Events(ctx, types.EventsOptions{})

	for {
		select {
		case event := <-eventCh:
			switch event.Type {
			case events.ContainerEventType:
				fmt.Printf("Evento de contenedor: %s %s\n", event.Action, event.Actor.ID)
			case events.ImageEventType:
				fmt.Printf("Evento de imagen: %s %s\n", event.Action, event.Actor.ID)
				if event.Action == "pull" {
					println("Image pull")
				}
			case events.NetworkEventType:
				fmt.Printf("Evento de red: %s %s\n", event.Action, event.Actor.ID)
			case events.VolumeEventType:
				fmt.Printf("Evento de volumen: %s %s\n", event.Action, event.Actor.ID)
			case events.ServiceEventType:
				fmt.Printf("Evento de servicio: %s %s\n", event.Action, event.Actor.ID)

			}
		case err := <-errCh:
			fmt.Printf("Error: %s", err)
			return err
		}
	}
}

// createBuildContext creates the context to build the image using the given DockerfileDataToCreateJenkinsImage data and returns the context as a io.Reader
func (s *DockerService) createBuildContext(dockerfileData *DockerfileDataToCreateJenkinsImage) io.Reader {
	// Crea un buffer para almacenar el contexto
	buf := new(bytes.Buffer)
	//gzipWriter := gzip.NewWriter(buf)
	// Crea un escritor TAR para el contexto
	tarWriter := tar.NewWriter(buf)

	// Añade los archivos al contexto desde templates
	s.AddFileFileToTarFromTemplate("Dockerfile", tarWriter, dockerfileData, 0640)
	s.AddFileFileToTarFromTemplate("settings.xml", tarWriter, nil, 0644)
	s.AddFileFileToTarFromTemplate("storage.conf", tarWriter, nil, 0640)
	s.AddFileFileToTarFromTemplate("containers.conf", tarWriter, nil, 0640)
	s.AddFileFileToTarFromTemplate(".bashrc", tarWriter, nil, 0640)

	// Cierra el escritor TAR
	if err := tarWriter.Close(); err != nil {
		panic(err)
	}

	println("Docker Context created")
	return bytes.NewReader(buf.Bytes())
}

// AddFileFileToTarFromTemplate adds a file to the given tarWriter from a template using the given data and the given mode for the file header in the tar file and panics if an error occurs during the process.
func (s *DockerService) AddFileFileToTarFromTemplate(name string, tarWriter *tar.Writer, data interface{}, mode int64) {
	// Render the Dockerfile template
	buf, err := s.TemplateFactory.RenderTemplate(name, data)
	if err != nil {
		fmt.Println("Error rendering template: " + name)
		panic(err)
	}

	// Crea la cabecera del archivo
	header := &tar.Header{
		Name: name,
		Size: int64(buf.Len()),
		Mode: mode,
	}

	// Escribe la cabecera en el archivo TAR
	if err := tarWriter.WriteHeader(header); err != nil {
		panic(err)
	}

	// Escribe el contenido del archivo en el archivo TAR
	if _, err := io.Copy(tarWriter, buf); err != nil {
		panic(err)

	}
}
