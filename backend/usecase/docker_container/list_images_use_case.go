package docker_container

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"strings"
)

// GetImagesByTagOrLabel returns a list of images that match the tag or label provided as an argument to the function call
func (s *DockerService) GetImagesByTagOrLabel(ctx context.Context, tagOrLabel string) ([]string, error) {

	images, err := s.Client.ImageList(ctx, types.ImageListOptions{All: true})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var imageList []string
	for _, image := range images {
		if strings.Contains(image.RepoTags[0], tagOrLabel) {
			imageList = append(imageList, image.RepoTags[0])
		}
		for label, value := range image.Labels {
			if strings.Contains(label, tagOrLabel) || strings.Contains(value, tagOrLabel) {
				imageList = append(imageList, image.ID)
			}
		}
	}
	return imageList, nil
}
