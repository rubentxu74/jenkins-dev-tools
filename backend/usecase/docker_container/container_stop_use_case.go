package docker_container

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
)

func (s *DockerService) StopContainer(ctx context.Context, containerName string) error {
	// Define un filtro para buscar el contenedor por nombre
	filterArgs := filters.NewArgs()
	filterArgs.Add("name", containerName)

	// Obtiene la lista de contenedores con el filtro aplicado
	containers, err := s.Client.ContainerList(ctx, types.ContainerListOptions{
		Filters: filterArgs,
	})
	if err != nil {
		return err
	}

	// Verifica si se encontró algún contenedor con el nombre especificado
	if len(containers) == 0 {
		return fmt.Errorf("No se encontró ningún contenedor con el nombre: %s", containerName)
	}

	// Detiene el primer contenedor encontrado con el nombre especificado
	err = s.Client.ContainerStop(ctx, containers[0].ID, container.StopOptions{})
	if err != nil {
		return err
	}

	return nil
}
