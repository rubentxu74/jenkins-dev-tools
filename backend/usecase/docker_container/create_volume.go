package docker_container

import (
	"context"
	"github.com/docker/docker/api/types/volume"
)

func (s *DockerService) CreateOrReuseVolume(ctx context.Context, volumeName string) (string, error) {
	volume, err := s.Client.VolumeCreate(ctx, volume.CreateOptions{
		Name: volumeName,
	})

	if err != nil {
		return "", err
	}

	return volume.Name, nil
}