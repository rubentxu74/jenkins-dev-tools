package docker_container

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
)

type ContainerOptions struct {
	ContainerName    string
	Config           *container.Config
	HostConfig       *container.HostConfig
	NetworkingConfig *network.NetworkingConfig
}

func (s *DockerService) RunContainer(ctx context.Context, containerOptions *ContainerOptions) error {

	containerResp, err := s.Client.ContainerCreate(ctx, containerOptions.Config, containerOptions.HostConfig, containerOptions.NetworkingConfig, nil, containerOptions.ContainerName)
	if err != nil {
		return err
	}

	if err := s.Client.ContainerStart(ctx, containerResp.ID, types.ContainerStartOptions{}); err != nil {
		return err
	}

	return nil
}
