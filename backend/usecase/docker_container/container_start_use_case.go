package docker_container

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"io"
	"os"
)

// ContainerStart starts a container
// It returns an error if one occurs
// It returns nil if no error occurs
func (s *DockerService) ContainerStart(ctx context.Context, imageName string) error {

	out, err := s.Client.ImagePull(ctx, imageName, types.ImagePullOptions{})
	if err != nil {
		return err
	}
	defer out.Close()
	io.Copy(os.Stdout, out)

	resp, err := s.Client.ContainerCreate(ctx, &container.Config{
		Image: imageName,
	}, nil, nil, nil, "")
	if err != nil {
		return err
	}

	if err := s.Client.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return err
	}

	fmt.Println(resp.ID)
	return nil
}
