package docker_container

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"strings"
)

// GetContainersByNameOrLabel returns a list of containers that match the name or label provided as an argument to the function call
func (s *DockerService) GetContainersByNameOrLabel(ctx context.Context, nameOrLabel string) ([]string, error) {

	containers, err := s.Client.ContainerList(ctx, types.ContainerListOptions{All: true})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var matchingContainers []string = []string{}
	for _, container := range containers {
		for _, name := range container.Names {
			if name == "/"+nameOrLabel {
				matchingContainers = append(matchingContainers, name)
				break
			}
		}
		for labelKey, labelValue := range container.Labels {
			if strings.Contains(labelKey, nameOrLabel) || strings.Contains(labelValue, nameOrLabel) {
				matchingContainers = append(matchingContainers, container.Names[0])
			}
		}
	}

	return matchingContainers, nil
}

// GetContainersByImageBaseName returns a list of containers that match the image base name provided as an argument to the function call
func (s *DockerService) GetContainersByImageBaseName(ctx context.Context, baseName string) ([]string, error) {

	containers, err := s.Client.ContainerList(ctx, types.ContainerListOptions{All: true})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var matchingContainers []string = []string{}
	for _, container := range containers {
		if strings.Contains(container.Image, baseName) {
			matchingContainers = append(matchingContainers, container.Names[0])
		}
	}

	return matchingContainers, nil
}
