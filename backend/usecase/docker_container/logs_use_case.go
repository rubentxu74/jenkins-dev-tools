package docker_container

import (
	"context"
	"github.com/docker/docker/api/types"
	"io"
	"os"
)

// ContainerLogs returns the logs of a container with the given ID as a string
func (s *DockerService) ContainerLogs(containerId string) {
	ctx := context.Background()

	options := types.ContainerLogsOptions{ShowStdout: true}
	// Replace this ID with a container that really exists
	out, err := s.Client.ContainerLogs(ctx, containerId, options)
	if err != nil {
		panic(err)
	}

	io.Copy(os.Stdout, out)
}
