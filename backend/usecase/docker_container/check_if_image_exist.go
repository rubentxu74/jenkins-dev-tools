package docker_container

import "context"

// CheckIfImageExist checks if an image exists in the local docker registry
// It returns true if the image exists, false otherwise
func (s *DockerService) CheckIfImageExist(ctx context.Context, tagImage string) bool {
	images, _ := s.GetImagesByTagOrLabel(ctx, tagImage)
	if len(images) > 0 {
		return true
	}
	return false
}
