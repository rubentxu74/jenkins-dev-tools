package docker_container

import (
	"github.com/docker/docker/client"
	"rubentxu.dev/jenkins_dev_container/cdi"
	"rubentxu.dev/jenkins_dev_container/usecase/templates"
)

const DockerServiceName = "docker"

// DockerService represents a service for interacting with the Docker API.
type DockerService struct {
	Client          *client.Client
	TemplateFactory *templates.TemplateFactory
}

// NewDockerService creates a new instance of the DockerService struct, and returns a pointer to it.
//
// factory: A pointer to an instance of the TemplateFactory struct.
//
// Returns a pointer to a DockerService struct, and an error if one occurs.
//
// This function creates a new instance of the DockerService struct,
// and initializes its Client field by calling client.NewClientWithOpts with the client.FromEnv
// and client.WithAPIVersionNegotiation options. The TemplateFactory field is set to the value
// passed in as an argument.
func NewDockerService(factory *templates.TemplateFactory) (*DockerService, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}

	return &DockerService{
		Client:          cli,
		TemplateFactory: factory,
	}, nil
}

func (s *DockerService) Config() error {
	return nil
}

func GetDockerService() *DockerService {
	service, ok := cdi.GetServiceLocator().GetService(DockerServiceName)
	if !ok {
		panic("Docker Service not found")
	}
	return service.(*DockerService)
}
