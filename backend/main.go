package main

import (
	"fmt"
	"github.com/markbates/pkger"
	"net"
	"os"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"rubentxu.dev/jenkins_dev_container/cdi"
	"rubentxu.dev/jenkins_dev_container/infra/controllers"
	"rubentxu.dev/jenkins_dev_container/infra/handlers"
	"rubentxu.dev/jenkins_dev_container/usecase/docker_container"
	"rubentxu.dev/jenkins_dev_container/usecase/jenkins"
	"rubentxu.dev/jenkins_dev_container/usecase/templates"

	"github.com/gofiber/fiber/v2/middleware/logger"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		println("Error loading .env file")
	}

	server := fiber.New(fiber.Config{
		Prefork:       false,
		CaseSensitive: true,
		StrictRouting: true,
	})

	server.Use(logger.New(logger.Config{
		Format: "[${ip}]:${port} ${status} - ${method} ${path}\n",
	}))

	//server.HTTPErrorHandler = func(err error, c echo.Context) {
	//	c.JSON(http.StatusNotFound, handlers.HTTPMessageBody{Message: "Not Found"})
	//}
	server.Use("/ws", func(c *fiber.Ctx) error {
		// IsWebSocketUpgrade returns true if the client
		// requested upgrade to the WebSocket protocol.
		if websocket.IsWebSocketUpgrade(c) {
			c.Locals("allowed", true)
			clientID := c.Query("X-Client-ID", "anonymous")
			if clientID == "" {
				return nil
			}
			return c.Next()
		}
		return fiber.ErrUpgradeRequired
	})

	intializeServices()
	configureHandlers(server)
	initializeServer(server)

}

func configureJenkinsTemplates() *templates.TemplateFactory {
	// Jenkins templates
	templates_path := os.Getenv("TEMPLATES_PATH")
	path := pkger.Dir(templates_path)
	templateFactory := templates.NewTemplateFactory(string(path))
	templateFactory.AddTemplatesFromFile("HealthCheck", "jenkins/base_script.tmpl", "jenkins/health_check_script.tmpl")
	templateFactory.AddTemplatesFromFile("InstallPlugins", "jenkins/base_script.tmpl", "jenkins/install_plugins_script.tmpl")
	templateFactory.AddTemplatesFromFile("CreateEnvargs", "jenkins/base_script.tmpl", "jenkins/create_envargs_script.tmpl")
	templateFactory.AddTemplatesFromFile("InstallAsdf", "jenkins/base_script.tmpl", "jenkins/install_asdf_script.tmpl")
	templateFactory.AddTemplatesFromFile("CreateUser", "jenkins/base_script.tmpl", "jenkins/create_user_script.tmpl")
	templateFactory.AddTemplatesFromFile("CreateCredentials", "jenkins/base_script.tmpl", "jenkins/create_credentials-script.tmpl")
	templateFactory.AddTemplatesFromFile("GetInstalledPlugins", "jenkins/base_script.tmpl", "jenkins/get_installed_plugins_script.tmpl")

	// Docker templates
	templateFactory.AddTemplateFromFile("Dockerfile", "docker/dockerfile_base.tmpl")
	templateFactory.AddTemplateFromFile("settings.xml", "docker/maven_settings.tmpl")
	templateFactory.AddTemplateFromFile("storage.conf", "docker/storage-conf.tmpl")
	templateFactory.AddTemplateFromFile("containers.conf", "docker/containers-conf.tmpl")
	templateFactory.AddTemplateFromFile(".bashrc", "docker/.bashrc.tmpl")
	return templateFactory
}

func intializeServices() {
	templateFactory := configureJenkinsTemplates()
	cdi.GetServiceLocator().RegisterService(templates.TemplateFactoryName, templateFactory)

	jenkinsServices := jenkins.NewJenkinsServices()
	cdi.GetServiceLocator().RegisterService(jenkins.JenkinsServiceName, jenkinsServices)

	dockerServices, _ := docker_container.NewDockerService(templateFactory)

	cdi.GetServiceLocator().RegisterService(docker_container.DockerServiceName, dockerServices)

	manager := controllers.NewWebSocketConnectionManager()
	go manager.Start()

	cdi.GetServiceLocator().RegisterService(controllers.WebSocketManager, manager)

}

func configureHandlers(server *fiber.App) {

	server.Get("/check", handlers.Check)
	script_handler := handlers.ExecuteRemoteScriptInJenkins()
	server.Get("/ws/rpc/script", script_handler)

	listJenkinsImages := handlers.GetImagesByTagOrLabel()
	server.Get("/api/container/image/jenkins", listJenkinsImages)

	executeAction := handlers.ExecuteAction()
	server.Get("/ws/rpc/action", executeAction)

	is_dev, err := strconv.ParseBool(os.Getenv("IS_DEV"))
	if err == nil && !is_dev {
		web_path := os.Getenv("WEB_PATH")
		path := pkger.Dir(web_path)
		server.Static("/", string(path))
	}

}

func initializeServer(server *fiber.App) {
	socket_address_type := os.Getenv("SOCKET_ADDRESS_TYPE")
	if socket_address_type == "unix" {
		socketPath := "/run/guest-jenkins/backend2.sock"
		println("Starting listening on %s\n", socketPath)
		unixListener, err := listen(socketPath)
		if err != nil {
			println("error listening to unix socket: %v", err)
		}
		defer unixListener.Close()
		server.Listener(unixListener)
		server.Listen(":")
	} else {
		port := os.Getenv("PORT")
		println(server.Listen(fmt.Sprintf(":%s", port)))
	}
}

func listen(path string) (net.Listener, error) {
	return net.Listen("unix", path)
}
