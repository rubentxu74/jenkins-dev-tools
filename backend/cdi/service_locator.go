package cdi

import (
	"sync"
)

// AppService es una interfaz que deben implementar los servicios de la aplicación.
type AppService interface {
	GetKey() string
}

// ServiceLocator es una estructura que almacena instancias de servicios de la aplicación.
type ServiceLocator struct {
	services map[string]interface{}
	lock     sync.Mutex
}

var serviceLocator *ServiceLocator
var once sync.Once

// GetServiceLocator devuelve una instancia de ServiceLocator (singleton).
func GetServiceLocator() *ServiceLocator {
	once.Do(func() {
		serviceLocator = &ServiceLocator{
			services: make(map[string]interface{}),
		}
	})
	return serviceLocator
}

// RegisterService registra una instancia de un servicio de la aplicación con el ServiceLocator.
func (s *ServiceLocator) RegisterService(name string, service interface{}) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.services[name] = service
}

// GetService devuelve una instancia del servicio de la aplicación con el nombre especificado.
func (s *ServiceLocator) GetService(name string) (interface{}, bool) {
	s.lock.Lock()
	defer s.lock.Unlock()

	service, ok := s.services[name]
	return service, ok
}
