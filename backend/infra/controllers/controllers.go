package controllers

import (
	"github.com/gofiber/websocket/v2"
	"rubentxu.dev/jenkins_dev_container/cdi"
	"sync"
)

const WebSocketManager = "websocket"

type WebSocketConnectionManager struct {
	Clients           map[*websocket.Conn]bool
	Register          chan *websocket.Conn
	Unregister        chan *websocket.Conn
	clientConnections map[string]*websocket.Conn
	clientMutex       sync.Mutex
}

func NewWebSocketConnectionManager() *WebSocketConnectionManager {
	return &WebSocketConnectionManager{
		Clients:           make(map[*websocket.Conn]bool),
		Register:          make(chan *websocket.Conn),
		Unregister:        make(chan *websocket.Conn),
		clientConnections: make(map[string]*websocket.Conn),
	}
}

func (m *WebSocketConnectionManager) Start() {
	for {
		select {
		case conn := <-m.Register:
			m.clientMutex.Lock()
			if _, ok := m.clientConnections[conn.Query("X-Client-ID", "anonymous")]; !ok {
				m.Clients[conn] = true
				m.clientConnections[conn.Query("X-Client-ID", "anonymous")] = conn
			}
			m.clientMutex.Unlock()
		case conn := <-m.Unregister:
			if _, ok := m.Clients[conn]; ok {
				delete(m.Clients, conn)
				delete(m.clientConnections, conn.Query("X-Client-ID", "anonymous"))
				conn.Close()
			}
		}
	}
}

func GetWebsocketManager() *WebSocketConnectionManager {
	service, ok := cdi.GetServiceLocator().GetService(WebSocketManager)
	if !ok {
		panic("WebsocketManager not found")
	}
	return service.(*WebSocketConnectionManager)
}
