package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"golang.org/x/net/context"
	"math"
	"rubentxu.dev/jenkins_dev_container/usecase/jenkins"
	"strings"
)

func processMessage(conn *websocket.Conn, err error, message []byte, jenkins *jenkins.JenkinsServices, messageType int) (bool, error) {
	// Convertir el mensaje a un mapa
	messageMap := make(map[string]interface{})
	err = json.Unmarshal(message, &messageMap)
	if err != nil {
		return false, err
	}

	// Obtener el valor del campo "type"
	typeMessage, ok := messageMap["type"].(string)
	if !ok {
		return false, fmt.Errorf("Type not found")
	}
	payload, _ := messageMap["payload"].(map[string]interface{})

	if strings.TrimSpace(typeMessage) == "Script" {
		payload["EnpointBackend"] = "ws://localhost:8081/ws/rpc/script?client_id=67890"

		script, ok := messageMap["script"].(string)
		if !ok {
			return false, fmt.Errorf("Script field not found")
		}

		_, err := jenkins.ExecuteGlobalRemoteScript(script, 5, payload)

		if err != nil {
			return false, err
		}
		return true, nil
	}
	return false, nil

}

func GetPaginatedAvailablePlugins(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error {
	// Recuperar los parámetros de paginación

	jenkins := jenkins.GetJenkinsService()
	pluginList, err := jenkins.GetPlugins()

	if err != nil {
		return err
	}

	page := int(payload["page"].(float64))
	pageSize := int(payload["pageSize"].(float64))
	totalPages := int(math.Ceil(float64(len(pluginList)) / float64(pageSize)))
	fmt.Println("totalPages: ", totalPages)

	// Calcular el índice del primer y último elemento en la página actual
	startIndex := (page - 1) * pageSize
	endIndex := startIndex + pageSize
	if endIndex > len(pluginList) {
		endIndex = len(pluginList)
	}
	plugins := pluginList[startIndex:endIndex]
	fmt.Println("plugins size: ", len(plugins))

	ValidResponse(conn, fiber.Map{
		"totalPages": totalPages,
		"plugins":    plugins,
	})
	return nil
}
