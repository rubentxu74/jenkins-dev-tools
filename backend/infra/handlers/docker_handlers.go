package handlers

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/go-connections/nat"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"io"
	"regexp"
	"rubentxu.dev/jenkins_dev_container/usecase/docker_container"
	"strconv"
)

const jenkinsImageDomain = "jenkins-dev-container"

func extractIdBuild(text string) (string, bool) {
	// Define el patrón regex
	pattern := regexp.MustCompile(`Successfully built (.*)`)

	// Busca el patrón regex en el texto
	matches := pattern.FindStringSubmatch(text)

	// Si hay una coincidencia, extrae el ID de la imagen
	if len(matches) > 1 {
		imageID := matches[1]
		return imageID, true
	} else {
		return "", false
	}
}

func CreateImage(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error {
	docker := docker_container.GetDockerService()
	id, installPodman, installPython, noCache, done := resolveParams(conn, payload)
	if done {
		return fmt.Errorf("Not resolved query params")
	}
	tagImage := payload["id"].(string)
	containerImage := fmt.Sprintf("%s:%s", jenkinsImageDomain, tagImage)
	fmt.Printf("\nCreating image %s\n", containerImage)

	dockerfileData := &docker_container.DockerfileDataToCreateJenkinsImage{
		ImageBase:     fmt.Sprintf("jenkins/jenkins:%s", id),
		AsdfDir:       "/var/jenkins_home/.asdf",
		AsdfDataDir:   "/opt/cache/jenkinsTools/.asdf-data",
		Shell:         "/bin/bash",
		Volume:        "/opt/libs",
		JenkinsTools:  "/opt/jenkins",
		InstallPodman: installPodman,
		InstallPython: installPython,
		NoCache:       noCache,
	}
	logs, err := docker.CreateImage(ctx, containerImage, dockerfileData)

	if err != nil {
		println("Error creating image: ", err.Error())
		return err
	}
	defer logs.Close()

	// Leer progresivamente de logs y enviarlo al cliente por websocket
	buf := make([]byte, 512)
	var idImage string = ""
	for {
		n, err := logs.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		if n > 0 {
			data := buf[:n]
			id, ok := extractIdBuild(string(data))
			if ok {
				ValidResponse(conn, fmt.Sprintf("DoneAction. Image created successfully. Image ID: %s", id))
				idImage = id
			} else {
				conn.WriteMessage(websocket.TextMessage, data)
			}
		}
	}
	if idImage == "" {
		ErrorResponse(conn, "ErrorAction. Image created failed")
	}
	return nil
}

func resolveParams(conn *websocket.Conn, payload map[string]interface{}) (string, bool, bool, bool, bool) {
	id := payload["id"].(string)
	if payload["installPodman"] == nil || payload["installPython"] == nil || payload["noCache"] == nil {
		ErrorResponse(conn, "Error Params: No installPodman or installPython or noCache provided")
		return "", false, false, false, true
	}
	installPodman := payload["installPodman"].(bool)
	installPython := payload["installPython"].(bool)
	noCache := payload["noCache"].(bool)
	fmt.Printf("id: %s, installPodman: %s, installPython: %s noCache: %s",
		id, strconv.FormatBool(installPodman), strconv.FormatBool(installPython), strconv.FormatBool(noCache))

	if id == "" {
		ErrorResponse(conn, "Error: No tag Container provided")
		return "", installPodman, installPython, noCache, true
	}

	return id, installPodman, installPython, noCache, false
}

func GetImagesByTagOrLabel() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		docker := docker_container.GetDockerService()
		tag := jenkinsImageDomain
		if tag == "" {
			ctx.Status(fiber.StatusBadRequest)
			return ctx.JSON(fiber.Map{
				"error": "No tag provided",
			})

		}
		context := context.Background()

		images, err := docker.GetImagesByTagOrLabel(context, tag)
		if err != nil {
			ctx.Status(fiber.StatusInternalServerError)
			return ctx.JSON(fiber.Map{
				"error": err.Error(),
			})
		}

		ctx.Status(fiber.StatusOK)
		return ctx.JSON(fiber.Map{
			"images": images,
		})

	}
}

func GetContainersByNameOrLabel(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error {

	docker := docker_container.GetDockerService()
	name, okName := payload["name"].(string)
	label, okLabel := payload["label"].(string)
	if !okName && !okLabel {
		ErrorResponse(conn, "No name or label provided")
	}
	context := context.Background()

	nameOrTag := name
	if name == "" {
		nameOrTag = label
	}

	containers, err := docker.GetContainersByNameOrLabel(context, nameOrTag)

	if err != nil {
		ErrorResponse(conn, err.Error())
	}

	ValidResponse(conn, fiber.Map{
		"containers": containers,
	})
	return nil
}

func GetContainerByImageBase(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error {

	docker := docker_container.GetDockerService()
	imageBase := payload["imageBase"].(string)
	if imageBase == "" {
		return fmt.Errorf("No imageBase provided")
	}
	context := context.Background()

	containers, err := docker.GetContainersByImageBaseName(context, imageBase)

	if err != nil {
		return err
	}

	ValidResponse(conn, fiber.Map{
		"containers": containers,
	})
	return nil

}

func RunJenkinsContainer(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error {
	var docker = docker_container.GetDockerService()
	volumeName := "jenkins_home"
	volume, err := docker.CreateOrReuseVolume(ctx, volumeName)
	if err != nil {
		return err
	}

	containerOptions, err := generateContainerOptions(volume, payload)
	if err != nil {
		return err
	}

	MessageResponse(conn, "Creating container...")
	result, _ := docker.GetContainersByNameOrLabel(ctx, containerOptions.ContainerName)

	if result != nil && len(result) == 0 {
		err = docker.RunContainer(ctx, containerOptions)
		if err != nil {
			return err
		}
	}

	ValidResponse(conn, "Container started")
	return nil
}

func StopJenkinsContainer(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error {
	var docker = docker_container.GetDockerService()
	containerName := payload["containerName"].(string)
	if containerName == "" {
		return fmt.Errorf("No containerName provided")
	}

	err := docker.StopContainer(ctx, containerName)

	if err != nil {
		return err
	}

	ValidResponse(conn, "Container stopped")
	return nil

}

func generateContainerOptions(jenkinsVolume string, payload map[string]interface{}) (*docker_container.ContainerOptions, error) {
	containerName := payload["containerName"].(string)
	imageName := payload["imageName"].(string)
	envs := payload["envs"].([]interface{})
	pathRepositories := payload["pathRepositories"].(string)
	//pathEnv := payload["pathEnv"].(string)
	hostPort := payload["hostPort"].(string)
	tag := payload["tag"].(string)

	containerPath := "/var/jenkins_home"

	var slice []string
	for _, v := range envs {
		if s, ok := v.(string); ok {
			slice = append(slice, s)
		}
	}

	keyPort, err := nat.NewPort("tcp", hostPort)
	if err != nil {
		return nil, err
	}
	config := &container.Config{
		Image: fmt.Sprintf("%s:%s", imageName, tag),
		Env:   slice,
		Volumes: map[string]struct{}{
			"/var/run/docker.sock": {},
			"jenkins-data":         {},
		},
		ExposedPorts: nat.PortSet{
			keyPort:              {},
			nat.Port("9000/tcp"): {},
		},
	}

	mounts := []mount.Mount{
		{
			Type:   mount.TypeBind,
			Source: pathRepositories,
			Target: "/opt/repositories",
		},
		{
			Type:        mount.TypeVolume,
			Source:      jenkinsVolume,
			Target:      containerPath,
			Consistency: mount.ConsistencyDefault,
		},
		//{
		//	Type:   mount.TypeBind,
		//	Source: pathEnv,
		//	Target: "/var/jenkins_home/.env",
		//},
	}

	portBindings := nat.PortMap{
		keyPort: []nat.PortBinding{{
			HostIP:   "0.0.0.0",
			HostPort: hostPort,
		}},
		nat.Port("9000/tcp"): []nat.PortBinding{{
			HostIP:   "0.0.0.0",
			HostPort: "9000",
		}},
	}

	hostConfig := &container.HostConfig{
		Mounts:     mounts,
		AutoRemove: true,
		Resources: container.Resources{
			Memory:   int64(3 * 1024 * 1024 * 1024),
			NanoCPUs: int64(2 * 1e9),
		},
		PortBindings:    portBindings,
		PublishAllPorts: false,
		Privileged:      true,
		NetworkMode:     "host",
	}

	_ = &network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			"host": {
				NetworkID: "host",
			},
		},
	}

	containerOptions := &docker_container.ContainerOptions{
		ContainerName:    containerName,
		Config:           config,
		HostConfig:       hostConfig,
		NetworkingConfig: nil,
	}
	return containerOptions, nil
}
