package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"log"
	"rubentxu.dev/jenkins_dev_container/infra/controllers"
	"rubentxu.dev/jenkins_dev_container/usecase/jenkins"
	"strings"
)

func ErrorResponse(conn *websocket.Conn, message string) error {
	fmt.Println("Error Response: ", message)
	return conn.WriteJSON(fiber.Map{
		"event":   "Error",
		"payload": message,
	})

}

func ValidResponse(conn *websocket.Conn, body interface{}) error {
	fmt.Println("Valid Response: ", body)
	return conn.WriteJSON(fiber.Map{
		"event":   "Success",
		"payload": body,
	})
}

func MessageResponse(conn *websocket.Conn, message interface{}) error {
	fmt.Println("Response Message: ", message)
	return conn.WriteJSON(fiber.Map{
		"event":   "Info",
		"payload": message,
	})
}

func Check(ctx *fiber.Ctx) error {
	return ctx.JSON(fiber.Map{"message": "Hello, World!"})
}

func ExecuteAction() fiber.Handler {
	return websocket.New(func(conn *websocket.Conn) {
		var (
			message []byte
			err     error
		)
		ctx, cancelFun := context.WithCancel(context.Background())
		defer cancelFun()
		defer conn.Close()

		manager := controllers.GetWebsocketManager()

		manager.Register <- conn
		defer func() {
			manager.Unregister <- conn
		}()
		MessageResponse(conn, fmt.Sprintf("Size clients: %d", len(manager.Clients)))

		for {
			if _, message, err = conn.ReadMessage(); err != nil {
				log.Println("read:", err)
				break
			}
			messageMap := make(map[string]interface{})
			fmt.Println("message: ", string(message))
			err = json.Unmarshal(message, &messageMap)
			if err != nil {
				ErrorResponse(conn, fmt.Sprintf("Error unmarshal message: %v", err))
				break
			}
			fmt.Printf("messageMap: %v	", messageMap)

			// Obtener el valor del campo "type"
			typeMessage, ok := messageMap["type"].(string)
			if !ok {
				ErrorResponse(conn, "Type not found")
				break
			}

			actionName, ok := messageMap["name"].(string)
			if !ok {
				ErrorResponse(conn, "Action name not found")
				break
			}

			// Si el campo "typeMessage" tiene el valor "Action", hacer una llamada a un servicio externo
			if strings.TrimSpace(typeMessage) == "Action" {

				payload, _ := messageMap["payload"].(map[string]interface{})
				action, err := resolveActionFuntion(actionName)
				if err != nil {
					ErrorResponse(conn, err.Error())
					break
				}

				err = action(conn, payload, ctx)
				if err != nil {
					ErrorResponse(conn, err.Error())
				}
				ValidResponse(conn, "DoneAction")
				break
			}
		}
	})
}

func resolveActionFuntion(action string) (func(conn *websocket.Conn, payload map[string]interface{}, ctx context.Context) error, error) {
	switch action {
	case "GetPaginatedAvailablePlugins":
		return GetPaginatedAvailablePlugins, nil
	case "RunJenkinsContainer":
		return RunJenkinsContainer, nil
	case "StopJenkinsContainer":
		return StopJenkinsContainer, nil
	case "CreateJenkinsImage":
		return CreateImage, nil
	case "GetContainersByNameOrLabel":
		return GetContainersByNameOrLabel, nil
	//case "ExecuteJenkinsScript":
	//	return ExecuteScrip, nil
	default:
		return nil, fmt.Errorf("Action not found")
	}
}

// ExecuteRemoteScriptInJenkins creates a new Fiber WebSocket handler for executing remote scripts
// in Jenkins. When a client connects, it registers the connection with the WebsocketManager and
// sends a message with the current number of connected clients. The handler listens for incoming
// messages, attempts to execute a script in Jenkins using the provided message, and sends a response
// back to the client indicating whether the execution was successful or not. If the script execution
// is successful or an error occurs, the connection is closed. If the execution is unsuccessful, the
// handler continues to listen for new messages. The received messages are also forwarded to all other
// connected clients.
func ExecuteRemoteScriptInJenkins() fiber.Handler {
	return websocket.New(func(conn *websocket.Conn) {
		jenkins := jenkins.GetJenkinsService()
		manager := controllers.GetWebsocketManager()

		manager.Register <- conn
		defer func() {
			manager.Unregister <- conn
		}()
		MessageResponse(conn, fmt.Sprintf("Size clients: %d", len(manager.Clients)))
		var (
			mt  int
			msg []byte
			err error
		)
		for {
			if mt, msg, err = conn.ReadMessage(); err != nil {
				break
			}

			ok, err := processMessage(conn, err, msg, jenkins, mt)
			if ok {
				ValidResponse(conn, "Done execute script")
				break
			} else if err != nil {
				ErrorResponse(conn, err.Error())
				break
			}
			sendMessageToClients(conn, mt, msg, manager)

		}
	})

}

// sendMessageToClients forwards a given message to all other connected clients in the WebSocketConnectionManager,
// excluding the sender. The function takes the sender's connection, the message type, the message bytes, and a reference
// to the WebSocketConnectionManager. If an error occurs while writing the message to a client, the function logs the error
// and stops sending the message to the remaining clients.
func sendMessageToClients(conn *websocket.Conn, mt int, msg []byte, manager *controllers.WebSocketConnectionManager) {
	for client, ok := range manager.Clients {
		if client != conn && ok {
			if err := client.Conn.WriteMessage(mt, msg); err != nil {
				log.Printf("Error al escribir mensaje: %v", err)
				break
			}
		}
	}
}
