FROM golang:1.19-alpine AS builder
ENV CGO_ENABLED=0
WORKDIR /backend
COPY backend/go.* .
RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go mod download
COPY backend/. .
RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go build -trimpath -ldflags="-s -w" -o bin/service


FROM --platform=$BUILDPLATFORM ubuntu:22.04 AS client-builder
RUN apt-get update
RUN apt-get install -y curl git wget unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa fonts-droid-fallback lib32stdc++6 python3
RUN apt-get clean
RUN git clone https://github.com/flutter/flutter.git /usr/local/flutter
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"
RUN flutter doctor
RUN flutter channel master
RUN flutter upgrade
RUN flutter config --enable-web
WORKDIR /ui
COPY ui /ui
RUN flutter build web --web-renderer canvaskit --release


FROM nginx:1.23.3-alpine
LABEL org.opencontainers.image.title="Jenkins Dev Container" \
    org.opencontainers.image.description="Contenedor para desarrollo de Jenkins" \
    org.opencontainers.image.vendor="Rubentxu" \
    com.docker.desktop.extension.api.version="0.3.3" \
    com.docker.extension.screenshots="" \
    com.docker.extension.detailed-description="" \
    com.docker.extension.publisher-url="" \
    com.docker.extension.additional-urls="" \
    com.docker.extension.changelog=""


# RUN apk add --no-cache --update openrc netcat-openbsd && \
#     mkdir -p /run/guest-services 



# Crea un directorio para el usuario y le da permisos
RUN mkdir -p /home/developer/app /var/cache/nginx/ && \
    chown -R nginx:nginx /home/developer/app /var/cache/nginx/ /var/run/
    

# Instalamos supervisord
RUN apk add --no-cache supervisor



COPY --chown=nginx:nginx --from=builder /backend/bin/service /home/developer/app/service
COPY --chown=nginx:nginx --from=builder /backend/resources /home/developer/app/resources
# COPY docker-compose.yaml .
# COPY metadata.json .
# COPY logo.svg .
# Copiamos el archivo de configuración de supervisord
COPY --chown=nginx:nginx supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY --chown=nginx:nginx nginx.conf /etc/nginx/conf.d/default.conf
# COPY --chown=developer:root --from=client-builder /ui/build/web/ /usr/share/nginx/html
COPY --chown=nginx:nginx --from=client-builder /ui/docker-desktop-ui/ /usr/share/nginx/html

# RUN setcap 'cap_net_bind_service=+ep' /usr/sbin/nginx /app/service
WORKDIR /home/developer/app
USER nginx
# Puerto que expondrá el contenedor
EXPOSE 8082
EXPOSE 8081


# Comando para iniciar supervisord
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

